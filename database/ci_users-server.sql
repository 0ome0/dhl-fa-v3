-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 07, 2018 at 10:20 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dhl_fa-v3`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_users`
--

CREATE TABLE `ci_users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `center_code` varchar(5) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobile_no` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` tinyint(4) NOT NULL DEFAULT '1',
  `is_active` tinyint(4) NOT NULL DEFAULT '1',
  `is_admin` tinyint(4) NOT NULL DEFAULT '0',
  `last_ip` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_users`
--

INSERT INTO `ci_users` (`id`, `username`, `center_code`, `firstname`, `lastname`, `email`, `mobile_no`, `password`, `role`, `is_active`, `is_admin`, `last_ip`, `created_at`, `updated_at`) VALUES
(3, 'admin@admin.com', 'RMT', 'System', 'Admin', 'akarapon.n@gmail.com', '12345', '$2y$10$kqiwRiCcIBsS/i0FC9k3YOE.sSVgu/PKCcO.baV8T4EDru4.qMXrS', 1, 1, 1, '', '2017-09-29 10:09:44', '2017-10-17 10:10:55'),
(30, 'account_admin@demo.com', '', 'Account Admin', 'Requestor', 'akarapon.n@gmail.com', '', '$2y$10$XKudM.Q39bjH8aQdmkfAUeE2YeVX1DAKlXN9KZ/lyDUwawgA7Grqm', 3, 1, 0, '', '2017-12-06 04:12:59', '2018-01-07 02:01:15'),
(154, 'it_admin@demo.com', '', 'IT Dept', 'Admin', 'akarapon.n@gmail.com', '', '$2y$10$N4QYZwowx3Qdu.wLeCye0.Kii6Ysz3NlhKG59YcP0SezVhHAmJp0q', 4, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(155, 'gdr.seksit.khamkhosit@dhl.com', 'gdr', 'GDR Loc', 'Manager', 'akarapon.n@gmail.com', '', '$2y$10$7qvILJJhKkwq5Al9KvdEceNKf1Q.wzNLz5LG4Y8FtX8g1kavG//3e', 5, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(150, 'country_mgr@demo.com', '', 'Country Manager', 'Demouser', 'akarapon.n@gmail.com', '', '$2y$10$N4QYZwowx3Qdu.wLeCye0.Kii6Ysz3NlhKG59YcP0SezVhHAmJp0q', 12, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(151, 'seksit.Khamkhosit@dhl.com', 'rmt', 'RMT Loc', 'Admin', 'akarapon.n@gmail.com', '', '$2y$10$N4QYZwowx3Qdu.wLeCye0.Kii6Ysz3NlhKG59YcP0SezVhHAmJp0q', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(152, 'navarat.siriphairoj@dhl.com', '', 'Head of Finance', 'Demouser', 'akarapon.n@gmail.com', '', '$2y$10$N4QYZwowx3Qdu.wLeCye0.Kii6Ysz3NlhKG59YcP0SezVhHAmJp0q', 6, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(153, 'grd_loc_admin@demo.com', 'gdr', 'GDR Loc', 'Admin', 'akarapon.n@gmail.com', '', '$2y$10$N4QYZwowx3Qdu.wLeCye0.Kii6Ysz3NlhKG59YcP0SezVhHAmJp0q', 2, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(156, 'rmt.seksit.khamkhosit@dhl.com', 'rmt', 'RMT Loc', 'Manager', 'akarapon.n@gmail.com', '', '$2y$10$N4QYZwowx3Qdu.wLeCye0.Kii6Ysz3NlhKG59YcP0SezVhHAmJp0q', 5, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(157, 'it_mgr@demo.com', '', 'IT Dept', 'Manager', 'akarapon.n@gmail.com', '', '$2y$10$N4QYZwowx3Qdu.wLeCye0.Kii6Ysz3NlhKG59YcP0SezVhHAmJp0q', 6, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(158, 'haruethai.chaicharoenporn@dhl.com', '', 'Account', 'Manager', 'akarapon.n@gmail.com', '', '$2y$10$N4QYZwowx3Qdu.wLeCye0.Kii6Ysz3NlhKG59YcP0SezVhHAmJp0q', 10, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(159, 'admin@demo.com', '', 'Admin', 'Purchasing', 'akarapon.n@gmail.com', '', '$2y$10$J8tWH6ksohZtv7duVMAHH.9sfpf.c5b0KvByWsEdZSLD/RwigjoX6', 13, 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_users`
--
ALTER TABLE `ci_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ci_users`
--
ALTER TABLE `ci_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 07, 2018 at 03:10 PM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dhl_fa_v3`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_user_groups`
--

CREATE TABLE `ci_user_groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(100) NOT NULL,
  `level_approve` int(1) NOT NULL DEFAULT '0',
  `approve_min` int(10) NOT NULL,
  `approve_max` int(11) NOT NULL,
  `approve` tinyint(1) NOT NULL,
  `request` tinyint(1) NOT NULL DEFAULT '0',
  `user_approve_id` int(10) NOT NULL,
  `setting` tinyint(1) NOT NULL DEFAULT '0',
  `approve_by_role` tinyint(1) DEFAULT '0',
  `role_admin` tinyint(1) NOT NULL DEFAULT '0',
  `role_it` tinyint(1) NOT NULL DEFAULT '0',
  `role_approve` tinyint(1) NOT NULL DEFAULT '0',
  `role_request` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_user_groups`
--

INSERT INTO `ci_user_groups` (`id`, `group_name`, `level_approve`, `approve_min`, `approve_max`, `approve`, `request`, `user_approve_id`, `setting`, `approve_by_role`, `role_admin`, `role_it`, `role_approve`, `role_request`) VALUES
(1, 'System Admin', -1, 0, 0, 0, 1, 0, 1, 0, -1, 0, 0, 0),
(2, 'Location Admin', 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 1),
(3, 'Account Admin', 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1),
(4, 'IT Admin', 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1),
(5, 'location Manager', 2, 0, 390000, 1, 0, 0, 0, 0, 0, 0, 1, 0),
(6, 'IT Manager', 3, 0, 390000, 1, 0, 157, 0, 0, 0, 1, 1, 0),
(10, 'Account Manager', 4, 0, 390000, 1, 0, 158, 0, 0, 0, 0, 1, 0),
(11, 'Head of Finance', 6, 0, 390000, 1, 0, 152, 0, 0, 0, 0, 1, 0),
(12, 'Country Manager', 5, 390001, 99999999, 1, 0, 150, 0, 0, 0, 0, 1, 0),
(13, 'Admin', 1, 0, 390000, 1, 0, 0, 0, 0, 0, 0, 1, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_user_groups`
--
ALTER TABLE `ci_user_groups`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ci_user_groups`
--
ALTER TABLE `ci_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

1. v_costcenter_mapping 
CREATE view v_costcenter_mapping as 

SELECT t1.costcenter_id, t1.costcenter_name,
t2.country_id, t2.country_name , t2.user_country_manager,
t3.department_id, t3.function_description, t3.function_name ,t3.user_department_head,
t4.sc_id ,t4.sc_name , t4.user_location_admin

FROM `t_costcenter` as t1
left outer join t_country as t2 on left(t1.costcenter_id,4) = t2.country_id 
left outer join t_department t3 on right(left(t1.costcenter_id,7),3) = t3.department_id
left outer join t_servicecenter t4 on right(t1.costcenter_id,3) = t4.sc_id 

-----------------------------------------------
2. v_role_approve
CREATE view v_role_approve as 
select t1.req_number, t1.req_type ,t1.asset_id ,t1.submit_status,
t2.id, t2.group_name, t2.approve_min, t2.approve_max, t2.approve,
t3.asset_class ,t3.book_val, t3.costcenter,
t4.it_approve,

CASE
   -- WHEN t3.book_val BETWEEN t2.approve_min and t2.approve_max THEN "1"
    WHEN ( t3.book_val >= t2.approve_max ) or ( t3.book_val between t2.approve_min and t2.approve_max ) THEN "1"
    ELSE "0"
END as role_approve

from t_request as t1
join ci_user_groups t2
left join t_fa t3 on t1.asset_id = t3.asset_id
left join t_asset_class t4 on t3.asset_class = t4.asset_class_id

where t2.approve = 1
order by t1.req_number , t2.id

--------------------------------------------------
3. v_role_approve_user
create view v_role_approve_user AS
SELECT t1.trans_id, t1.req_number, t1.submit_status, t1.role_id, t1.approve_min, t1.approve_max, t1.book_val,
 t1.it_approve,t1.approve_status , 

t2.asset_id ,t3.asset_no, t3.asset_description,
t3.costcenter, t2.to_costcenter,


case 
when t1.role_id = '1' then 'System Admin'
when t1.role_id = '2' then t4.user_location_admin
when t1.role_id = '3' then 'Account Admin'
when t1.role_id = '4' then t4.user_department_head
when t1.role_id = '5' then t4.user_country_manager
when t1.role_id = '6' then 'Head of Finance'
else 'Please Contact System Admin'
end as user_approver
FROM t_approve_level as t1 
left outer join t_request as t2 on t1.req_number = t2.req_number
left outer join t_fa as t3 on t2.asset_id = t3.asset_id 
left outer join v_costcenter_mapping as t4 on t3.costcenter = t4.costcenter_id

---------------------------------------------------
4. v_current_approver
create view v_current_approver as
select req_number , min(role_id) as current_role_approve,t2.group_name as role, 'review' as review
from t_approve_level t1 left outer join ci_user_groups t2 on t1.role_id = t2.id 

where approve_status = '1'
and req_number not in 
(select req_number from t_approve_level where approve_status = 3)
group by req_number 

5. v_request_step
create view v_request_step as
select 
t1.req_number , count(t1.approve_status) as approve_step
from t_approve_level as t1
group by t1.req_number

6. v_request_reject
create view v_request_reject as
select 
t2.req_number , count(t2.approve_status) as reject
from t_approve_level as t2
where approve_status = 3
group by t2.req_number

7. v_request_approved
create view v_request_approved as
select 
t3.req_number , count(t3.approve_status) as approved
from t_approve_level as t3
where approve_status <> 1
group by t3.req_number

8. v_request_status
create view v_request_status as
SELECT v1.req_number , v1.approve_step, v2.reject , v3.approved,
case 
when v2.reject is not null then '3'
when v1.approve_step = v3.approved then '2'
else '1'
end as req_status

from v_request_step as v1

left outer join v_request_reject as v2
on v1.req_number = v2.req_number

left outer join v_request_approved as v3
on v1.req_number = v3.req_number

9. ci_users_menu
create view v_ci_users_menu as
select t1.id , t1.username,  t1.center_code, t1.firstname ,t1.lastname,t1.email,t1.mobile_no,t1.password 
,t1.role, t2.group_name,t2.approve_min,t2.approve_max,t2.approve ,t2.request,t2.setting
,t1.is_active,t1.is_admin,
case
when t2.approve = '1' and t2.request = '1' and t2.setting = '1' then '1'
when t2.approve = '0' and t2.request = '0' and t2.setting = '1' then '2'
when t2.approve = '1' and t2.request = '1' and t2.setting = '0' then '3'
when t2.approve = '1' and t2.request = '0' and t2.setting = '0' then '4'
when t2.approve = '0' and t2.request = '1' and t2.setting = '0' then '5'
else '0'
end as menu_level
,t2.approve_by_role

from ci_users as t1 
left outer join ci_user_groups as t2
on t1.role = t2.id

10. v_asset_by_dept_by_req
create view  v_asset_by_dept_by_req as
select t1.asset_id, t1.asset_no, t1.asset_description ,t1.invent_no , t1.costcenter  ,t1.asset_class
, left(t1.costcenter,4) as cost_country , right(left(t1.costcenter,7),3) as cost_dept , right(t1.costcenter,3) as cost_location 
, t2.req_number , t2.it_asset as req_it_asset
, t3.it_approve as fa_it_asset
from t_fa as t1
inner join t_request_header as t2 on right(left(t1.costcenter,7),3) = t2.dept_id
left outer join t_asset_class as t3 on t1.asset_class = t3.asset_class_id 
where 
t2.it_asset = t3.it_approve

v_approver_list
SELECT 
t1.req_number, t1.req_type ,t1.req_type , t1.it_asset,t1.submit_status,t1.request_date,t1.user_request,t1.dept_id,
t2.id,t2.group_name,t2.level_approve,t2.approve_min,t2.approve_max ,t2.approve_by_role
FROM t_request_header as t1
join ci_user_groups as t2
where t1.req_number = 8
and  t2.role_approve = 1 and ( t2.level_approve >= 2 or t1.it_asset = t2.role_it )


//// view for api

10. v_costcenter
create view v_costcenter as 
SELECT t1.costcenter_id, t1.costcenter_name,
concat(t2.country_name,"-",t3.function_description,"-",t3.function_name,"-",t4.sc_name) as costcenter_description

FROM `t_costcenter` as t1
left outer join t_country as t2 on left(t1.costcenter_id,4) = t2.country_id 
left outer join t_department t3 on right(left(t1.costcenter_id,7),3) = t3.department_id
left outer join t_servicecenter t4 on right(t1.costcenter_id,3) = t4.sc_id
where t1.status = 1

11. v_request_header
create view v_request_header as 
select 
t1.req_number , t1.req_type , t3.type_name as req_type_description
, t1.to_costcenter , t1.dept_id , t1.it_asset ,t1.submit_status , t2.submit_description
, t1.request_status , t4.approve_status_description as request_status_description
, t1.request_date
, t1.user_request 
, t1.ref_uniqid

from t_request_header as t1
left join t_submit_status as t2 on t1.submit_status = t2.submit_id
left join t_request_type as t3 on t1.req_type = t3.type_id
left join t_approve_status as t4 on t1.request_status = t4.status_id

12. v_request_detail
create view v_request_detail as 
SELECT t1.request_detail_id , t1.request_no as req_num , t1.asset_id , t1.net_value
, t2.qr_code_id, t2.asset_image, t2.asset_no,t2.asset_description,t2.sno,t2.invent_no
,t2.costcenter, t2.dep_key, t2.asset_class, t2.cap_date, t2.acquis_val
,t2.accum_dep, t2.book_val, t2.currency
from t_request_detail as t1
left join t_fa as t2 on t1.asset_id = t2.asset_id

13. v_it_asset
create view v_it_asset as
SELECT t1.asset_id,t1.qr_code_id, t1.qr_code_img, t1.asset_image, t1.asset_description, t1.sno
, t1.invent_no, t1.costcenter, t1.dep_key, t1.asset_class, t1.cap_date, t1.acquis_val, t1.accum_dep
, t1.book_val, t1.currency, t1.created_date

, t2.asset_bs_account, t2.asset_class_name, t2.it_approve as it_asset_class
, right(left(t1.costcenter,6),3) as function_name
, case 
when t2.it_approve = '1' or right(left(t1.costcenter,6),3) in ('690','770','860') then '1'
else '0'
end as it_asset
, right(t1.costcenter,3) as service_center , t3.sc_name
from t_fa as t1
left outer join t_asset_class as t2 
on t1.asset_class = t2.asset_class_id
left outer join t_servicecenter as t3 on right(t1.costcenter,3) = t3.sc_id

14. v_request_total_value
create view v_request_total_value as
SELECT request_no as req_number , sum(net_value) as total_value FROM t_request_detail
group by request_no

/* *************** old flow not use ********
15. v_flow_approve old flow
create view v_flow_approve as
SELECT 
t1.req_number, t1.req_type , t1.it_asset,t1.submit_status,t1.request_date,t1.user_request,t1.dept_id, t2.id,t2.group_name
, case  when t2.user_approve_id = 0 then t5.user_location_manager
        else t2.user_approve_id
        end as user_approve_id
, t2.level_approve,t2.approve_min,t2.approve_max 
, t3.role
, case 	when t3.role = 2 and t2.level_approve = 1 then 1 
		when t3.role = 2 and t2.level_approve = 2 then 0
        when t3.role = 2 and t2.level_approve = 3 then 1
        when t3.role = 2 and t2.level_approve = 4 then 1
        when t3.role = 2 and t2.level_approve = 5 and t2.approve_min <= t4.total_value then 1
        
        when t3.role = 3 and t2.level_approve = 1 then 0
		when t3.role = 3 and t2.level_approve = 2 then 0
        when t3.role = 3 and t2.level_approve = 3 then 1
        when t3.role = 3 and t2.level_approve = 4 then 1
        when t3.role = 3 and t2.level_approve = 5 and t2.approve_min <= t4.total_value then 1
        
        when t3.role = 4 and t2.level_approve = 1 then 0
		when t3.role = 4 and t2.level_approve = 2 then 1
        when t3.role = 4 and t2.level_approve = 3 then 1
        when t3.role = 4 and t2.level_approve = 4 then 1
        when t3.role = 4 and t2.level_approve = 5 and t2.approve_min <= t4.total_value then 1
        
  		else 0
  end as flow_flag
, t4.total_value  
FROM t_request_header as t1
left outer join ci_users as t3 on t1.user_request = t3.id
left outer join v_request_total_value as t4 on t1.req_number = t4.req_number
join ci_user_groups as t2
left outer join t_servicecenter as t5 on t1.user_request = t5.user_location_admin
where  
t2.role_approve = 1
*/

15. v_flow_approve new flow
create view v_flow_approve as
SELECT 
t1.req_number, t1.req_type , t1.it_asset,t1.submit_status,t1.request_date,t1.user_request,t1.dept_id, t2.id,t2.group_name
, case  when t2.user_approve_id = 0 then t5.user_location_manager
        else t2.user_approve_id
        end as user_approve_id
, t2.level_approve,t2.approve_min,t2.approve_max 
, t3.role
, case 	when t3.role = 2 and t2.level_approve = 1 then 1
		when t3.role = 2 and t2.level_approve = 2 then 1
        when t3.role = 2 and t2.level_approve = 3 then 0
        when t3.role = 2 and t2.level_approve = 4 then 0
        when t3.role = 2 and t2.level_approve = 5 and t2.approve_min <= t4.total_value then 1
        
        when t3.role = 3 and t2.level_approve = 1 then 1
		when t3.role = 3 and t2.level_approve = 2 then 0
        when t3.role = 3 and t2.level_approve = 3 then 0
        when t3.role = 3 and t2.level_approve = 4 then 1
        when t3.role = 3 and t2.level_approve = 5 and t2.approve_min <= t4.total_value then 1
        
        when t3.role = 4 and t2.level_approve = 1 then 1
		when t3.role = 4 and t2.level_approve = 2 then 0
        when t3.role = 4 and t2.level_approve = 3 then 1
        when t3.role = 4 and t2.level_approve = 4 then 0
        when t3.role = 4 and t2.level_approve = 5 and t2.approve_min <= t4.total_value then 1
        
  		else 0
  end as flow_flag
, t4.total_value  
FROM t_request_header as t1
left outer join ci_users as t3 on t1.user_request = t3.id
left outer join v_request_total_value as t4 on t1.req_number = t4.req_number
join ci_user_groups as t2
left outer join t_servicecenter as t5 on t1.user_request = t5.user_location_admin
where  
t2.role_approve = 1

16. v_request_current_approve
create view v_request_current_approve as
select t1.req_number , min(t1.level_approve) as current_role_approve 
, t2.group_name as role
, t1.user_approve_id
, t3.username, t3.firstname, t3.lastname, t3.email
,'review' as review

from t_request_approve_list as t1

left outer join ci_user_groups t2 on t1.role_approver = t2.id 
left outer join ci_users as t3 on t1.user_approve_id = t3.id

where t1.approve_status = '1'
and t1.req_number not in 
(select req_number from t_request_approve_list where approve_status = 3)
group by req_number



17. v_request_approve_list
create view v_request_approve_list as
SELECT t1.req_number, t1.req_type 
, t1.user_request_id 
, t1.role_requestor 
, t1.user_approve_id , t3.username , t3.firstname , t3.lastname ,t3.email
, t1.level_approve , t4.group_name
, t1.approve_status , t2.approve_status_description
, t1.approve_date
, case when t1.approve_status = 1 then ''
  when t1.approve_status in ('2','3') then t1.approve_date
  else t1.approve_date
  end as approved_date
FROM t_request_approve_list as t1
left outer join t_approve_status as t2 on t1.approve_status = t2.status_id
left outer join ci_users as t3 on t1.user_approve_id = t3.id
left outer join ci_user_groups as t4 on t1.role_approver = t4.id
order by t1.level_approve asc


18. v_request_header_show
create view v_request_header_show as 
select t1.req_number,t1.req_type ,t1.req_type_description as request_type, t1.to_costcenter
, t1.submit_status , t1.submit_description as submit
, t1.request_status as request_status,t1.request_status_description as approve
, t1.request_date , t1.user_request
, t2.username, t2.firstname, t2.lastname, t2.role as role_id, t3.group_name as role
,  IFNULL(t4.total_value, 0) as total_value
from v_request_header as t1
left outer join ci_users as t2 on t1.user_request = t2.id
left outer join ci_user_groups as t3 on t2.role = t3.id
left outer join v_request_total_value as t4 on t1.req_number = t4.req_number


19. v_for_insert_history_log
alter view v_for_insert_history_log as 
SELECT t1.req_number , t1.req_type
, t3.costcenter as from_costcenter , t1.to_costcenter as to_costcenter
, date(t1.request_date) as request_date , curdate() as approved_date, t1.user_request as request_by, t2.asset_id
, t2.net_value
, concat(t1.req_number,"-",t1.req_type,"-",t2.asset_id) as pk
from t_request_header as t1
left outer join t_request_detail as t2 on t1.req_number = t2.request_no
left outer join t_fa as t3 on t2.asset_id = t3.asset_id
where t1.submit_status <> '1' and t2.asset_id is not null
ORDER by t1.req_number


20. v_history_log
alter view v_history_log as 
SELECT t1.trans_id,t1.req_number, t1.req_type , t3.type_name
, t1.from_costcenter, t1.to_costcenter, t1.request_date, t1.approved_date
, t1.request_by, t1.asset_id, t1.net_value
, t2.asset_no, t2.asset_description, t2.sno,t2.invent_no,t2.dep_key, t2.asset_class, t2.cap_date, t2.acquis_val 
, t2.accum_dep , t2.currency
FROM t_history_log as t1
left outer join t_fa as t2 on t1.asset_id = t2.asset_id
left outer join t_request_type as t3 on t1.req_type = t3.type_id


// query for clear data for test again
// query for clear data to start production


////////////////// Import Asset Data /////////////////////

/// view for import data
1. query for old asset but not exist in new data ( flag disposal or delete )
alter view v_import_1_old_asset_notin_new_asset as
select t1.*
from t_fa as t1
left outer join t_fa_import as t2 on t1.asset_no = t2.asset_no
where t2.asset_no is null

2. query for old asset equal new data ( update book value )
alter view v_import_2_new_asset_update_old_asset as
select t2.*
from t_fa as t1
left join t_fa_import as t2 on t1.asset_no = t2.asset_no
where t1.asset_no = t2.asset_no

3. query for new data not exist in old data ( insert new asset )
alter view v_import_3_new_data_for_insert as
select t1.*
from t_fa_import as t1
left outer join t_fa as t2 on t1.asset_no = t2.asset_no
where t2.asset_no is null




// query for case 1 delete flag old asset
update t_fa as t1
inner join v_import_1_old_asset_notin_new_asset as t2 on t1.asset_no = t2.asset_no
set t1.disposal_status = 1
where t1.asset_no = t2.asset_no

// query for case 2 update book value old asset
update t_fa as t1
inner join v_import_2_new_asset_update_old_asset as t2 on t1.asset_no = t2.asset_no
set 
t1.asset_description = t2.asset_description , t1.sno = t2.asset_no, t1.invent_no = t2.invent_no , t1.costcenter = t2.costcenter
, t1.dep_key = t2.dep_key, t1.asset_class = t2.asset_class, t1.cap_date = t2.cap_date, t1.acquis_val = t2.acquis_val
, t1.accum_dep = t2.accum_dep , t1.book_val = t2.book_val , t1.currency = t2.currency 
where t1.asset_no = t2.asset_no

// query for case 3  insert new asset
insert into t_fa ( asset_no, asset_description, sno, invent_no, costcenter
, dep_key, asset_class, cap_date, acquis_val, accum_dep, book_val, currency )
select t2.asset_no, t2.asset_description, t2.sno, t2.invent_no, t2.costcenter, t2.dep_key, t2.asset_class, t2.cap_date,
t2.acquis_val, t2.accum_dep, t2.book_val, t2.currency 
from v_import_3_new_data_for_insert as t2

select * from t_fa where qr_code_id is null
2148-2172

2403-2453

localhost/dhl-fa-v3/api/Qr_scan/gen_newqr
-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 21, 2018 at 08:05 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dhl_fa`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_request`
--

CREATE TABLE `t_request` (
  `req_number` int(10) NOT NULL,
  `req_type` int(10) NOT NULL,
  `asset_id` int(11) NOT NULL,
  `to_costcenter` varchar(12) NOT NULL,
  `submit_status` int(2) NOT NULL DEFAULT '1',
  `request_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_request` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_request`
--

INSERT INTO `t_request` (`req_number`, `req_type`, `asset_id`, `to_costcenter`, `submit_status`, `request_date`, `user_request`) VALUES
(26, 1, 1, '6601080588', 2, '2018-07-21 15:03:15', 'admin@admin.com'),
(27, 1, 3, '6601170588', 2, '2018-07-21 15:03:41', 'admin@admin.com'),
(28, 2, 6, '', 2, '2018-07-21 15:03:56', 'admin@admin.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_request`
--
ALTER TABLE `t_request`
  ADD PRIMARY KEY (`req_number`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_request`
--
ALTER TABLE `t_request`
  MODIFY `req_number` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

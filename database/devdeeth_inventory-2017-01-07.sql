-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 07, 2018 at 05:01 PM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devdeeth_inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_transfer`
--

CREATE TABLE `t_transfer` (
  `transfer_id` int(10) NOT NULL,
  `trans_type_code` varchar(20) NOT NULL,
  `trans_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `item_id` int(13) NOT NULL,
  `item_code` varchar(20) NOT NULL,
  `item_um` int(5) NOT NULL,
  `qty_transfer` int(11) NOT NULL,
  `from_whse` varchar(20) NOT NULL,
  `qty_received` int(11) NOT NULL,
  `to_whse` varchar(20) NOT NULL,
  `ref_document` varchar(20) NOT NULL,
  `note` text NOT NULL,
  `username` varchar(20) NOT NULL,
  `transfer_status` varchar(20) NOT NULL DEFAULT 'Pending',
  `received_status` varchar(20) NOT NULL,
  `received_date` datetime NOT NULL,
  `username_received` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_transfer`
--

INSERT INTO `t_transfer` (`transfer_id`, `trans_type_code`, `trans_date`, `item_id`, `item_code`, `item_um`, `qty_transfer`, `from_whse`, `qty_received`, `to_whse`, `ref_document`, `note`, `username`, `transfer_status`, `received_status`, `received_date`, `username_received`) VALUES
(33, 'to', '2017-12-30 19:16:24', 1, '', 0, 10, 'RMT', 10, 'APD', 'TEST0111', '<p>\r\n	fse</p>\r\n', 'Admin', 'Pending', 'Received', '2017-12-30 19:17:04', 'Test@test.com'),
(34, 'to', '2018-01-07 20:59:48', 1, '', 0, 12, 'APD', 12, 'RMT', 'ef', '<p>\r\n	fes</p>\r\n', 'Test@test.com', 'Pending', 'Received', '2018-01-07 20:59:59', 'Admin'),
(35, 'to', '2018-01-07 21:19:35', 1, '', 0, 12, 'APD', 12, 'RMT', 'test', '<p>\r\n	test</p>\r\n', 'Test@test.com', 'Pending', 'Received', '2018-01-07 21:19:46', 'Admin'),
(36, 'to', '2018-01-07 23:37:16', 1, '', 0, 11, 'APD', 11, 'RMT', 'test', '<p>\r\n	set</p>\r\n', 'Admin', 'Pending', '', '0000-00-00 00:00:00', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_transfer`
--
ALTER TABLE `t_transfer`
  ADD PRIMARY KEY (`transfer_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_transfer`
--
ALTER TABLE `t_transfer`
  MODIFY `transfer_id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

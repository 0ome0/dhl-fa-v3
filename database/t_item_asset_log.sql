-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 15, 2018 at 07:42 AM
-- Server version: 5.6.34-log
-- PHP Version: 7.1.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `devdeeth_qr-inventory`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_item_asset_log`
--

CREATE TABLE `t_item_asset_log` (
  `id` int(11) NOT NULL,
  `asset_id` int(10) NOT NULL,
  `asset_name` varchar(255) NOT NULL,
  `asset_description` text NOT NULL,
  `serial_number` varchar(100) NOT NULL,
  `asset_code_number` varchar(100) NOT NULL,
  `asset_image` varchar(255) NOT NULL,
  `asset_type` int(5) NOT NULL,
  `location` varchar(5) NOT NULL,
  `installation_date` date NOT NULL,
  `purchase_date` date NOT NULL,
  `expire_date` date NOT NULL,
  `condition_warranty` text NOT NULL,
  `reference_document` varchar(255) NOT NULL,
  `vendor_contact` int(5) NOT NULL,
  `use_status` int(5) NOT NULL,
  `asset_owner` int(10) NOT NULL,
  `qr_code_id` varchar(255) NOT NULL,
  `qr_code_img` varchar(255) NOT NULL,
  `last_updated` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user_updated` varchar(255) NOT NULL,
  `bu_name` varchar(255) DEFAULT NULL,
  `owner` varchar(255) DEFAULT NULL,
  `os_name` varchar(255) DEFAULT NULL,
  `os_version` varchar(255) DEFAULT NULL,
  `os_revision` varchar(255) DEFAULT NULL,
  `cpu_type` varchar(255) DEFAULT NULL,
  `hdd` varchar(255) DEFAULT NULL,
  `ram` varchar(255) DEFAULT NULL,
  `in_ad_notin_altiris` varchar(255) DEFAULT NULL,
  `monitor_model_1` varchar(255) DEFAULT NULL,
  `monitor_serial_number_1` varchar(255) DEFAULT NULL,
  `monitor_model_2` varchar(255) DEFAULT NULL,
  `monitor_serial_number_2` varchar(255) DEFAULT NULL,
  `delivery_date` date DEFAULT NULL,
  `expired_date` date DEFAULT NULL,
  `purchasing_status` varchar(255) DEFAULT NULL,
  `contract_number` varchar(255) DEFAULT NULL,
  `lease_end_date` date DEFAULT NULL,
  `po_leasing_number` varchar(255) DEFAULT NULL,
  `unit_cost` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `t_item_asset_log`
--

INSERT INTO `t_item_asset_log` (`id`, `asset_id`, `asset_name`, `asset_description`, `serial_number`, `asset_code_number`, `asset_image`, `asset_type`, `location`, `installation_date`, `purchase_date`, `expire_date`, `condition_warranty`, `reference_document`, `vendor_contact`, `use_status`, `asset_owner`, `qr_code_id`, `qr_code_img`, `last_updated`, `user_updated`, `bu_name`, `owner`, `os_name`, `os_version`, `os_revision`, `cpu_type`, `hdd`, `ram`, `in_ad_notin_altiris`, `monitor_model_1`, `monitor_serial_number_1`, `monitor_model_2`, `monitor_serial_number_2`, `delivery_date`, `expired_date`, `purchasing_status`, `contract_number`, `lease_end_date`, `po_leasing_number`, `unit_cost`) VALUES
(14, 46, 'THBKKWDA1020133', '<p>\n	Optiplex 7010</p>\n', 'BX89F22', '2430003560', '', 1, '5', '0000-00-00', '0000-00-00', '2018-09-05', '', '', 6, 1, 429, '5B0535A60AE05', '5B0535A60AE05.png', '2018-06-15 13:29:02', '', 'TH_Express', 'DHL', 'Windows 7 Enterprise', '6.1', 'Service Pack 1', '0', '237568', '4096', '> 60 Days', 'Dell1740', 'CND7011258', NULL, NULL, '2014-09-04', '2018-09-05', 'Purchasing', NULL, NULL, NULL, 2222.00),
(15, 37, 'THBKKWDA1040076', '<p>\n	OptiPlex 9020</p>\n', '12YJS62', '2430003803', '', 1, '5', '2018-06-24', '2018-06-06', '2019-08-07', '', '', 6, 1, 208, '5B0535A5A2D66', '5B0535A5A2D66.png', '2018-06-15 13:37:58', '', 'TH_Express', 'DHL', 'Windows 7 Enterprise', '6.1', 'Service Pack 1', 'Intel(R) Core(TM) i7-4790 CPU @ 3.60GHz', '304128', '4096', '1-30 Days', 'Dell 2014Ht', 'CN01D64V74445563746L', NULL, NULL, '2015-08-06', '2019-08-07', 'Purchasing', NULL, '2018-06-27', NULL, 11.00),
(16, 37, 'THBKKWDA1040076', '<p>\n	OptiPlex 9020</p>\n', '12YJS62', '2430003803', '', 1, '5', '2018-06-24', '2018-06-06', '2019-08-07', '', '', 6, 1, 208, '5B0535A5A2D66', '5B0535A5A2D66.png', '2018-06-15 13:39:24', 'admin@admin.com', 'TH_Express', 'DHL', 'Windows 7 Enterprise', '6.1', 'Service Pack 1', 'Intel(R) Core(TM) i7-4790 CPU @ 3.60GHz', '304128', '4096', '1-30 Days', 'Dell 2014Ht', 'CN01D64V74445563746L', NULL, NULL, '2015-08-06', '2019-08-07', 'Purchasing', NULL, '2018-06-27', NULL, 22.00),
(17, 37, 'THBKKWDA1040076', '<p>\n	OptiPlex 9020</p>\n', '12YJS62', '2430003803', '', 1, '5', '2018-06-24', '2018-06-06', '2019-08-07', '', '', 6, 1, 208, '5B0535A5A2D66', '5B0535A5A2D66.png', '2018-06-15 14:08:51', 'admin@admin.com', 'TH_Express', 'DHL', 'Windows 7 Enterprise', '6.1', 'Service Pack 1', 'Intel(R) Core(TM) i7-4790 CPU @ 3.60GHz', '304128', '4096', '1-30 Days', 'Dell 2014Ht', 'CN01D64V74445563746L', NULL, NULL, '2015-08-06', '2019-08-07', 'Purchasing', NULL, '2018-06-27', NULL, 2.00),
(18, 37, 'THBKKWDA1040076', '<p>\n	OptiPlex 9020</p>\n', '12YJS62', '2430003803', '', 1, '5', '2018-06-24', '2018-06-06', '2019-08-07', '', '', 6, 1, 208, '5B0535A5A2D66', '5B0535A5A2D66.png', '2018-06-15 14:24:39', 'admin@admin.com', 'TH_Express', 'DHL', 'Windows 7 Enterprise', '6.1', 'Service Pack 1', 'Intel(R) Core(TM) i7-4790 CPU @ 3.60GHz', '304128', '4096', '1-30 Days', 'Dell 2014Ht', 'CN01D64V74445563746L', NULL, NULL, '2015-08-06', '2019-08-07', 'Purchasing', NULL, '2018-06-27', NULL, 99.00),
(19, 37, 'THBKKWDA1040076', '<p>\n	OptiPlex 9020</p>\n', '12YJS62', '2430003803-1', '', 1, '5', '2018-06-24', '2018-06-06', '2019-08-07', '', '', 6, 1, 208, '5B0535A5A2D66', '5B0535A5A2D66.png', '2018-06-15 14:25:48', 'admin@admin.com', 'TH_Express', 'DHL', 'Windows 7 Enterprise', '6.1', 'Service Pack 1', 'Intel(R) Core(TM) i7-4790 CPU @ 3.60GHz', '304128', '4096', '1-30 Days', 'Dell 2014Ht', 'CN01D64V74445563746L', NULL, NULL, '2015-08-06', '2019-08-07', 'Purchasing', NULL, '2018-06-27', NULL, 99.00),
(20, 37, 'THBKKWDA1040076', '<p>\n	OptiPlex 9020</p>\n', '12YJS62', '2430003803-111', '', 1, '5', '2018-06-24', '2018-06-06', '2019-08-07', '', '', 6, 1, 208, '5B0535A5A2D66', '5B0535A5A2D66.png', '2018-06-15 14:26:40', 'admin@admin.com', 'TH_Express', 'DHL', 'Windows 7 Enterprise', '6.1', 'Service Pack 1', 'Intel(R) Core(TM) i7-4790 CPU @ 3.60GHz', '304128', '4096', '1-30 Days', 'Dell 2014Ht', 'CN01D64V74445563746L', NULL, NULL, '2015-08-06', '2019-08-07', 'Purchasing', NULL, '2018-06-27', NULL, 99.00),
(21, 37, 'THBKKWDA1040076', '<p>\n	OptiPlex 9020</p>\n', '12YJS62', '2430003803-111', '', 1, '1', '2018-06-24', '2018-06-06', '2019-08-07', '', '', 6, 1, 208, '5B0535A5A2D66', '5B0535A5A2D66.png', '2018-06-15 14:32:55', 'admin@admin.com', 'TH_Express', 'DHL', 'Windows 7 Enterprise', '6.1', 'Service Pack 1', 'Intel(R) Core(TM) i7-4790 CPU @ 3.60GHz', '304128', '4096', '1-30 Days', 'Dell 2014Ht', 'CN01D64V74445563746L', NULL, NULL, '2015-08-06', '2019-08-07', 'Purchasing', NULL, '2018-06-27', NULL, 99.00);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_item_asset_log`
--
ALTER TABLE `t_item_asset_log`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_item_asset_log`
--
ALTER TABLE `t_item_asset_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

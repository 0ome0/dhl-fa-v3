<?php
	class Auth_model extends CI_Model{

		// api function

		public function get_user_role($id){

			$sql = "select * from ci_users where id = '$id'";
			$query = $this->db->query($sql);
			
			if ($query->num_rows() == 0){
				return false;
			}
			else{
				return $result = $query->result_array();
			}
		}

		public function get_location($id){

			$sql = "select * from t_servicecenter where user_location_admin = '$id'";
			$query = $this->db->query($sql);

			if ($query->num_rows() == 0){
				return false;
			}
			else{
				return $result = $query->result_array();
			}

			
		}

		// end of api function

		public function login($data){
			// $query = $this->db->get_where('ci_users', array('email' => $data['email']));
			// $query = $this->db->get_where('ci_users', array('username' => $data['username']));
			$query = $this->db->get_where('v_ci_users_menu', array('username' => $data['username']));
			if ($query->num_rows() == 0){
				return false;
			}
			else{
				//Compare the password attempt with the password we have stored.
				$result = $query->row_array();
			    $validPassword = password_verify($data['password'], $result['password']);
			    if($validPassword){
			        return $result = $query->row_array();
			    }
				
			}
		}
		
		public function get_admin_detail(){
			$id = $this->session->userdata('admin_id');
			$query = $this->db->get_where('ci_users', array('id' => $id));
			return $result = $query->row_array();
		}

		public function update_admin($data){
			$id = $this->session->userdata('admin_id');
			$this->db->where('id', $id);
			$this->db->update('ci_users', $data);
			return true;
		}

		public function change_pwd($data, $id){
			$this->db->where('id', $id);
			$this->db->update('ci_users', $data);
			return true;
		}

		public function get_token_key(){

			$sql = "SELECT token_key FROM t_token";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
		}

	}

?>
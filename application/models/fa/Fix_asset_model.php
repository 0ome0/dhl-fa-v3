<?php
	class Fix_asset_model extends CI_Model{

		public function check_it_asset($qr_code_id){
			$sql = "select * from v_it_asset where qr_code_id = '$qr_code_id'";
			$query = $this->db->query($sql);
			
			if ($query->num_rows() == 0){
				return false;
			}
			else{
				return $result = $query->result_array();
			}
			
		}
		
		public function get_item_by_id($id){
			$query = $this->db->get_where('t_fa', array('asset_id' => $id));
			return $result = $query->result_array();
			
		}

		public function get_item_by_id_and_not_have_qr($id){
			$sql = "select * from t_fa where asset_id = '$id' and ( length(qr_code_id) < 2 or qr_code_id is null ) ";
			$query = $this->db->query($sql);
			
			return $result = $query->result_array();
			
		}

		public function get_item_asset_detail($qr_code_id){
			// $sql = "select * from v_fix_asset where qr_code_id = '$qr_code_id'";
			$sql = "select * from t_fa where qr_code_id = '$qr_code_id'";
            $query = $this->db->query($sql);
			// return $result = $query->row_array();
			return $result = $query->result_array();
			
		}

		public function get_location(){
			$sql = "select * from t_location where location_status = '1'";
            $query = $this->db->query($sql);
			// return $result = $query->row_array();
			return $result = $query->result_array();
			
		}

		public function get_asset_owner(){
			$sql = "select * from t_asset_owner where asset_owner_status = '1'";
            $query = $this->db->query($sql);
			// return $result = $query->row_array();
			return $result = $query->result_array();
			
		}

		public function get_usage_status(){
			$sql = "select * from t_use_status";
            $query = $this->db->query($sql);
			// return $result = $query->row_array();
			return $result = $query->result_array();
			
		}

		public function edit_item_asset($data, $qr_code_id){
			$this->db->where('qr_code_id', $qr_code_id);
			$this->db->update('t_item_asset', $data);
			return true;
		}
		
		public function insert_item_asset_log($asset_id,$username){

			$sql = " insert into t_item_asset_log SET

			user_updated = '$username'
			,  asset_id  = (select asset_id from t_item_asset where asset_id = '$asset_id' )
			,  asset_name = (select asset_name from t_item_asset where asset_id = '$asset_id')
			,  asset_description = (select asset_description from t_item_asset where asset_id = '$asset_id')
			,  serial_number = (select serial_number from t_item_asset where asset_id = '$asset_id')
			,  asset_code_number = (select asset_code_number from t_item_asset where asset_id = '$asset_id')
			,  asset_image = (select asset_image from t_item_asset where asset_id = '$asset_id')
			,  asset_type = (select asset_type from t_item_asset where asset_id = '$asset_id')
			,  location = (select location from t_item_asset where asset_id = '$asset_id')
			,  installation_date = (select installation_date from t_item_asset where asset_id = '$asset_id')
			,  purchase_date = (select purchase_date from t_item_asset where asset_id = '$asset_id')
			,  expire_date = (select expire_date from t_item_asset where asset_id = '$asset_id')
			,  condition_warranty = (select condition_warranty from t_item_asset where asset_id = '$asset_id')
			,  reference_document = (select reference_document from t_item_asset where asset_id = '$asset_id')
			,  vendor_contact = (select vendor_contact from t_item_asset where asset_id = '$asset_id')
			,  use_status = (select use_status from t_item_asset where asset_id = '$asset_id')
			,  asset_owner = (select asset_owner from t_item_asset where asset_id = '$asset_id')
			,  qr_code_id = (select qr_code_id from t_item_asset where asset_id = '$asset_id')
			,  qr_code_img = (select qr_code_img from t_item_asset where asset_id = '$asset_id') 
			,  bu_name = (select bu_name from t_item_asset where asset_id = '$asset_id')
			,  owner = (select owner from t_item_asset where asset_id = '$asset_id')
			,  os_name = (select os_name from t_item_asset where asset_id = '$asset_id')
			,  os_version = (select os_version from t_item_asset where asset_id = '$asset_id')
			,  os_revision = (select os_revision from t_item_asset where asset_id = '$asset_id')
			,  cpu_type = (select cpu_type from t_item_asset where asset_id = '$asset_id')
			,  hdd = (select hdd from t_item_asset where asset_id = '$asset_id')
			,  ram = (select ram from t_item_asset where asset_id = '$asset_id')
			,  in_ad_notin_altiris = (select in_ad_notin_altiris from t_item_asset where asset_id = '$asset_id')
			,  monitor_model_1 = (select monitor_model_1 from t_item_asset where asset_id = '$asset_id')
			,  monitor_serial_number_1 = (select monitor_serial_number_1 from t_item_asset where asset_id = '$asset_id')
			,  monitor_model_2 = (select monitor_model_2 from t_item_asset where asset_id = '$asset_id')
			,  monitor_serial_number_2 = (select monitor_serial_number_2 from t_item_asset where asset_id = '$asset_id')
			,  delivery_date = (select delivery_date from t_item_asset where asset_id = '$asset_id')
			,  expired_date = (select expired_date from t_item_asset where asset_id = '$asset_id')
			,  purchasing_status = (select purchasing_status from t_item_asset where asset_id = '$asset_id')
			,  contract_number = (select contract_number from t_item_asset where asset_id = '$asset_id')
			,  lease_end_date = (select lease_end_date from t_item_asset where asset_id = '$asset_id')
			,  po_leasing_number = (select po_leasing_number from t_item_asset where asset_id = '$asset_id')
			,  unit_cost= (select unit_cost from t_item_asset where asset_id = '$asset_id')
			";

            $query = $this->db->query($sql);
			// return $result = $query->row_array();
			// return $result = $query->result_array();
			return ;
			
		}

		public function insert_item_asset_log_by_qrcode($qr_code_id,$username){

			$sql = " insert into t_item_asset_log SET

			user_updated = '$username'
			,  asset_id  = (select asset_id from t_item_asset where qr_code_id = '$qr_code_id' )
			,  asset_name = (select asset_name from t_item_asset where qr_code_id = '$qr_code_id')
			,  asset_description = (select asset_description from t_item_asset where qr_code_id = '$qr_code_id')
			,  serial_number = (select serial_number from t_item_asset where qr_code_id = '$qr_code_id')
			,  asset_code_number = (select asset_code_number from t_item_asset where qr_code_id = '$qr_code_id')
			,  asset_image = (select asset_image from t_item_asset where qr_code_id = '$qr_code_id')
			,  asset_type = (select asset_type from t_item_asset where qr_code_id = '$qr_code_id')
			,  location = (select location from t_item_asset where qr_code_id = '$qr_code_id')
			,  installation_date = (select installation_date from t_item_asset where qr_code_id = '$qr_code_id')
			,  purchase_date = (select purchase_date from t_item_asset where qr_code_id = '$qr_code_id')
			,  expire_date = (select expire_date from t_item_asset where qr_code_id = '$qr_code_id')
			,  condition_warranty = (select condition_warranty from t_item_asset where qr_code_id = '$qr_code_id')
			,  reference_document = (select reference_document from t_item_asset where qr_code_id = '$qr_code_id')
			,  vendor_contact = (select vendor_contact from t_item_asset where qr_code_id = '$qr_code_id')
			,  use_status = (select use_status from t_item_asset where qr_code_id = '$qr_code_id')
			,  asset_owner = (select asset_owner from t_item_asset where qr_code_id = '$qr_code_id')
			,  qr_code_id = (select qr_code_id from t_item_asset where qr_code_id = '$qr_code_id')
			,  qr_code_img = (select qr_code_img from t_item_asset where qr_code_id = '$qr_code_id') 
			,  bu_name = (select bu_name from t_item_asset where qr_code_id = '$qr_code_id')
			,  owner = (select owner from t_item_asset where qr_code_id = '$qr_code_id')
			,  os_name = (select os_name from t_item_asset where qr_code_id = '$qr_code_id')
			,  os_version = (select os_version from t_item_asset where qr_code_id = '$qr_code_id')
			,  os_revision = (select os_revision from t_item_asset where qr_code_id = '$qr_code_id')
			,  cpu_type = (select cpu_type from t_item_asset where qr_code_id = '$qr_code_id')
			,  hdd = (select hdd from t_item_asset where qr_code_id = '$qr_code_id')
			,  ram = (select ram from t_item_asset where qr_code_id = '$qr_code_id')
			,  in_ad_notin_altiris = (select in_ad_notin_altiris from t_item_asset where qr_code_id = '$qr_code_id')
			,  monitor_model_1 = (select monitor_model_1 from t_item_asset where qr_code_id = '$qr_code_id')
			,  monitor_serial_number_1 = (select monitor_serial_number_1 from t_item_asset where qr_code_id = '$qr_code_id')
			,  monitor_model_2 = (select monitor_model_2 from t_item_asset where qr_code_id = '$qr_code_id')
			,  monitor_serial_number_2 = (select monitor_serial_number_2 from t_item_asset where qr_code_id = '$qr_code_id')
			,  delivery_date = (select delivery_date from t_item_asset where qr_code_id = '$qr_code_id')
			,  expired_date = (select expired_date from t_item_asset where qr_code_id = '$qr_code_id')
			,  purchasing_status = (select purchasing_status from t_item_asset where qr_code_id = '$qr_code_id')
			,  contract_number = (select contract_number from t_item_asset where qr_code_id = '$qr_code_id')
			,  lease_end_date = (select lease_end_date from t_item_asset where qr_code_id = '$qr_code_id')
			,  po_leasing_number = (select po_leasing_number from t_item_asset where qr_code_id = '$qr_code_id')
			,  unit_cost= (select unit_cost from t_item_asset where qr_code_id = '$qr_code_id')
			";

            $query = $this->db->query($sql);
			// return $result = $query->row_array();
			// return $result = $query->result_array();
			return ;
			
		}


		public function updated_item_asset_record($asset_id,$username,$last_updated){
			$sql = " update t_item_asset SET
			user_updated = '$username'
			,last_updated = '$last_updated'
			where asset_id = '$asset_id'
			";

            $query = $this->db->query($sql);
			// return $result = $query->row_array();
			// return $result = $query->result_array();
			return ;
		}

		public function updated_item_asset_record_by_qrcode ($qr_code_id,$username,$last_updated){

		
			$sql = " update t_item_asset SET
			user_updated = '$username'
			,last_updated = '$last_updated'
			where qr_code_id = '$qr_code_id'
			";

            $query = $this->db->query($sql);
			// return $result = $query->row_array();
			// return $result = $query->result_array();
			return ;
		}

		// get all asset will be expire in query days
		public function get_list_asset_expired_in_1(){
            $sql = "select * from  v_item_asset_expire_1";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

		public function get_list_asset_expired_in_2(){
            $sql = "select * from  v_item_asset_expire_2";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

         public function get_list_asset_expired_in_3(){
            $sql = "select * from  v_item_asset_expire_3";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

		// public function center_get_item_by_id($id){
		// 	$query = $this->db->get_where('v_item_whse', array('id' => $id));
		// 	return $result = $query->result_array();
			
		// }

		// public function get_item_by_whse_barcode($data){
		// 	$query = $this->db->get_where('v_item_whse', array('item_barcode' => $data['item_barcode'],'whse_center_code' => $data['whse_center_code'] ));
		// 	//$query = $this->db->get_where('v_item_whse');
		// 	return $result = $query->row_array();
			
		// }

		// public function get_stock_onhand_by_whse($whse){

		// 	$sql = "select * from v_item_whse where whse_center_code = '$whse' and qty_on_hand > '0' ";
        //     $query = $this->db->query($sql);
		// 	//return $result = $query->row_array();
		// 	return $result = $query->result_array();
		// }


	}

?>
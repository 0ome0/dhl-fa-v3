<?php
	class Dashboard_model extends CI_Model{

        // for fa-v3 
        public function get_count_total_asset_by_service_center(){
            $sql = "select count(asset_id) as total_asset 
            , IFNULL(v_it_asset.sc_name,' None') as service_center
            , service_center as center_code 
            from v_it_asset
            group by sc_name,service_center
            order by service_center";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_count_total_asset_all_service_center(){
            $sql = "select count(asset_id) as total_asset 
            from v_it_asset";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }


        public function get_count_request_transfer(){
            $sql = "SELECT count(req_number) as number_of_request ,submit_description
            ,req_type_description , request_status_description
            FROM `v_request_header`
            where req_type = '1'
            group by req_type_description,request_status_description ,submit_description";
            // order by submit_status,req_type asc";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_count_request_disposal(){
            $sql = "SELECT count(req_number) as number_of_request ,submit_description
            ,req_type_description , request_status_description
            FROM `v_request_header`
            where req_type = '2'
            group by  req_type_description,request_status_description ,submit_description";
            // order by submit_status,req_type asc";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_count_request_submit_status(){
            $sql = "SELECT count(req_number) as number_of_request
            ,submit_description , `request_status_description`
            FROM `v_request_header`
            group by submit_description , `request_status_description`";
            // order by submit_status asc";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        //
        
        public function get_item_list_by_whse($whse){
            $sql = "select count(*) as number_item from v_item_whse where whse_center_code = '$whse'";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }
        
        public function get_unassign_owner(){
            $sql = "select count(*) as number_unassign_item from t_item_asset where asset_owner = ''";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_assign_owner(){
            $sql = "select count(*) as number_assign_item from t_item_asset where asset_owner <> ''";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_itasset(){
            $sql = "select count(*) as number_itasset from t_item_asset";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_asset_owner(){
            $sql = "select count(*) as number_asset_owner from t_asset_owner";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_vendor(){
            $sql = "select count(*) as number_vendor from t_vendor";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        public function get_asset_expired(){
            $sql = "select count(*) as number_asset_expired from t_item_asset where expire_date < now()";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }
        
        // asset will be expire in 30 days
        public function get_asset_expired_in_1(){
            $sql = "select count(*) as number_asset_expired from t_item_asset where datediff(expire_date,now()) BETWEEN 0 and 30 ";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        // asset will be expire in 60 days
        public function get_asset_expired_in_2(){
            $sql = "select count(*) as number_asset_expired from t_item_asset where datediff(expire_date,now()) BETWEEN 31 and 60 ";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

        // asset will be expire in 90 days
        public function get_asset_expired_in_3(){
            $sql = "select count(*) as number_asset_expired from t_item_asset where datediff(expire_date,now()) BETWEEN 61 and 90 ";
            $query = $this->db->query($sql);
			return $result = $query->result_array();
        }

         // get all asset will be expire in query days
        //  public function get_asset_expired_in_3(){
        //     $sql = "select * from  number_asset_expired from t_item_asset where datediff(expire_date,now()) BETWEEN 61 and 90 ";
        //     $query = $this->db->query($sql);
		// 	return $result = $query->result_array();
        // }

    }

?>
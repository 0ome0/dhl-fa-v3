<!DOCTYPE html>

<html>

<head>

  <meta charset="utf-8" />

  <title>Email Alert from DHL Fix Asset Management System</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body>

<div class="container">
       <div class="row" style=text-align:center;><h1>DHL Fix Asset Management System</h1>
       <br>
       <h1 style="color:red;">Your Request has been reject</h1>
       </div>
        
        <h2>
        
       

        Request No. &nbsp;&nbsp;
        <?php foreach($req_header_description as $row): ?>
            <?= $row['req_number']; ?> 
            <br>
            Request date : <?= $row['request_date']; ?>
            <br>
            Request for : &nbsp;&nbsp;
            <?= $row['type_name']; ?>
            <br>
            To costcenter : &nbsp;&nbsp;
            <?= $row['to_costcenter']; ?>
            <br>
            Request from user : &nbsp;&nbsp;
            <?= $row['firstname']; ?>
            &nbsp;&nbsp;
            <?= $row['lastname']; ?>
        <?php endforeach; ?>
            <br>
        <?php foreach($req_total_value as $row): ?>
            Total Net Value : &nbsp;&nbsp;
            <?= $row['total_book_val']; ?> &nbsp;&nbsp; Baht.
            <br>
            
        <?php endforeach; ?>
        </h2>

        <hr>

        <div class="request_detail">
                <h2><u> Asset list in request document</u></h2>
                <TR>
                <p><b>
                <TD width="10%">Asset No | </TD>
                <TD width="10%">Asset Description | </TD>
                <TD width="10%">Sno. | </TD>
                <TD width="10%">Invent No. | </TD>
                <TD width="10%">Costcenter | </TD>
                <TD width="5%">Dep Key | </TD>
                <TD width="5%">Asset Class | </TD>
                <TD width="10%">Cap Date | </TD>
                <TD width="10%">Acquis Val | </TD>
                <TD width="10%">Accum Dep | </TD>
                <TD width="10%">Book Val </TD>
                </b>
                </p>
                </TR>

                <?php foreach($req_detail as $row): ?>
                <TR>
                <p>
                <TD width="10%"><?= $row['asset_no']; ?> &nbsp;|</TD>
                <TD width="10%"><?= $row['asset_description']; ?> &nbsp;|</TD>
                <TD width="10%"><?= $row['sno']; ?> &nbsp;|</TD>
                <TD width="10%"><?= $row['invent_no']; ?> &nbsp;|</TD>
                <TD width="10%"><?= $row['costcenter']; ?> &nbsp;|</TD>
                <TD width="5%"><?= $row['dep_key']; ?> &nbsp;|</TD>
                <TD width="5%"><?= $row['asset_class']; ?> &nbsp;|</TD>
                <TD width="10%"><?= $row['cap_date']; ?> &nbsp;|</TD>
                <TD width="10%"><?= $row['acquis_val']; ?> &nbsp;|</TD>
                <TD width="10%"><?= $row['accum_dep']; ?> &nbsp;|</TD>
                <TD width="10%"><?= $row['book_val']; ?> &nbsp;</TD>


                </TR>
                </p>
                <?php endforeach; ?>
              
        </div>

        <div class="approve_list">
                <h3><u> Approver List</u></h3>
                <!-- <TR>
                <h3><b>
                <TD width="20%">Role</TD>
                <TD width="20%">Namee</TD>
                <TD width="20%">Approve status</TD>
                <TD width="20%">Approve date</TD>
                </b>
                </h3>
                </TR> -->

                <?php foreach($approve_list as $row): ?>
                <TR>
                <p>
                <TD width="20%">Role : <?= $row['group_name']; ?> &nbsp;|</TD>
                <TD width="40%">Name : <?= $row['firstname']; ?> &nbsp; <?= $row['lastname']; ?> |&nbsp;</TD>
                <TD width="20%">Approve Status : <?= $row['approve_status_description']; ?> &nbsp;|</TD>
                <TD width="20%">Approve Date : <?= $row['approved_date']; ?> &nbsp;|</TD>
                </TR>
                </p>
                <?php endforeach; ?>
              
        </div>

        <hr>    
                
                   
                <br>
                <br> 
                <p>
                    Do not reply to this email.This email is automation send from DHL Fix Asset Management System.
                </p>

</div>
</body>

</html>
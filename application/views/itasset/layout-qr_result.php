<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>QR Code</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		  <!-- Bootstrap 3.3.6 -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/bootstrap/css/bootstrap.min.css">
		  <!-- Font Awesome -->
		  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
		  <!-- Ionicons -->
		  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		  <!-- Theme style -->
	      <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/AdminLTE.min.css">
	       <!-- Custom CSS -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/style.css">
		  <!-- AdminLTE Skins. Choose a skin from the css/skins. -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/skins/skin-blue.min.css">
		  <!-- jQuery 2.2.3 -->
		  <script src="<?= base_url() ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
		  <!-- jQuery UI 1.11.4 -->
		  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

</head>
<body>
            
           <div class="container" style="text-align:left;  font-size: large" >
            <div class=col-md-12>
                <div class="row" style="text-align:center">
                    <br>
                    <h1>Asset Information</h1>
                    <hr>
                </div>
                <div class="row">
                <br>
                    Asset Name : <?php foreach($item_detail as $row): ?>
                                        <?= $row['asset_name']; ?>
                                <?php endforeach; ?>

                </div>

                <div class="row">
                <br>
                    Asset Description : <?php foreach($item_detail as $row): ?>
                                        <?= $row['asset_description']; ?>
                                <?php endforeach; ?>
                </div>

                <div class="row">
                <br>
                    Serial No : <?php foreach($item_detail as $row): ?>
                                        <?= $row['serial_number']; ?>
                                <?php endforeach; ?>
                </div>

                <div class="row">
                <br>
                    Installation Date : <?php foreach($item_detail as $row): ?>
                                        <?= $row['installation_date']; ?>
                                <?php endforeach; ?>
                </div>

                <div class="row">
                <br>
                    Purchase Date : <?php foreach($item_detail as $row): ?>
                                        <?= $row['purchase_date']; ?>
                                <?php endforeach; ?>
                </div>

                <div class="row">
                <br>
                    Expire Date : <?php foreach($item_detail as $row): ?>
                                        <?= $row['expire_date']; ?>
                                <?php endforeach; ?>
                </div>

                <!-- <div class="row">
                    Condition warranty : <?php foreach($item_detail as $row): ?>
                                        <?= $row['condition_warranty']; ?>
                                <?php endforeach; ?>
                </div> -->

                
                <br>
            </div>
            <!-- <div class="col-md-1">
            </div> -->
            <!-- <div id = 'printarea'>
                    <div class="col-md-12" style="text-align:center">
                            <h1>
                            <?php foreach($item_detail as $row): ?>
                                <?= $row['asset_name']; ?>
                                <br>
                            
                            </h1>
                            <br>
                            <div id="print_qrcode" style="text-align:center" >
                            <div class="img" >
                                <img src="<?php echo base_url('assets/images/qrcode/')?><?php foreach($item_detail as $row): ?><?= trim($row['qr_code_img']); ?><?php endforeach; ?>" alt="" 
                                style="width:60%;"
                                >
                            </div>
                            </div>

                            <h1>
                            <?php endforeach; ?>
                                <br>
                                <?php foreach($item_detail as $row): ?>
                                    <?= trim($row['qr_code_id']); ?>
                                <?php endforeach; ?>
                            </h1>
                            <br>
                        
                    </div>
            </div>  -->
            <!-- end of print area -->
            <!-- <div class="col-md-1">
            </div> -->
           </div>
           <!-- <div class="container">
                <div class="col-md-12" style="text-align:center;">
                <input type='button'  class="btn btn-primary" id='btn' value='Print Barcode' onclick='printFunc();' style="width:50%;text-align:center;">
                </div>
            </div> -->
<!--                 
<script>
    function printFunc() {
    // var divToPrint = document.getElementById('printarea');
    var divToPrint = document.getElementById('print_qrcode');
    var htmlToPrint = '' +
        '<style type="text/css">' +
        'table th, table td {' +
        'border:1px solid #000;' +
        'padding;0.5em;' +
        '}' +
        '</style>';
    htmlToPrint += divToPrint.outerHTML;
    newWin = window.open("");
    newWin.document.write("<h3 align='center'>Print QR Code</h3>");
    newWin.document.write(htmlToPrint);
    newWin.print();
    newWin.close();
    }
</script> -->

</body>
</html>
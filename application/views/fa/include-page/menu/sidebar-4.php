<?php 
$cur_tab = $this->uri->segment(2)==''?'dashboard': $this->uri->segment(2);  
?>  

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less --> 
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url() ?>public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <br>
          <p><?= ucwords($this->session->userdata('name')); ?></p>
          <!-- <a href="#"><i class="fa fa-circle text-success"></i> <?= ucwords($this->session->userdata('whse')); ?></a> -->
        </div>
      </div>
      
     

      <ul class="sidebar-menu">
        <li class="header">Overview</li>
          <li><a href="<?= base_url('admin/dashboard/dashboard_admin'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Dashboard</span></a></li>
          <li><a href="<?= base_url('fa_con/fix_asset/fix_asset_management'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Fix Asset List</span></a></li>
          
          <!-- <li><a href="<?= base_url('fix-asset/item_asset/asset_expired'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Asset Expired</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Expire in  0-30 days</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Expire in 31-60 days</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Expire in 61-90 days</span></a></li> -->
      
       
          <li class="header">Transfer Request</li>
          <li><a href="<?= base_url('fa_con/request_new/transfer_request_status/1'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Draft</span></a></li>
          <li><a href="<?= base_url('fa_con/request_new/transfer_request_status/2'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Wait for Approve</span></a></li>
          <li><a href="<?= base_url('fa_con/request_new/transfer_request_status/3'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Approved</span></a></li>
          <li><a href="<?= base_url('fa_con/request_new/transfer_request_status/4'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Reject</span></a></li>
         
        
          <li class="header">Disposal Request</li>
          <li><a href="<?= base_url('fa_con/request_new/disposal_request_status/1'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Draft</span></a></li>
          <li><a href="<?= base_url('fa_con/request_new/disposal_request_status/2'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Wait for Approve</span></a></li>
          <li><a href="<?= base_url('fa_con/request_new/disposal_request_status/3'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Approved</span></a></li>
          <li><a href="<?= base_url('fa_con/request_new/disposal_request_status/4'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Reject</span></a></li>

       <!-- <li class="header">Approver</li>
          <li><a href="<?= base_url('fa_con/approve/wait_for_approve'); ?>/<?= ucwords($this->session->userdata('approve_by_role')); ?>/<?= ucwords($this->session->userdata('role')); ?>/<?= ucwords($this->session->userdata('user_id')); ?> "><i class="fa fa-circle-o text-red"></i> <span>Wait to approve</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Approved</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Reject</span></a></li> -->
          

     <!-- <li class="header">Limit Approve Level</li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Department Head</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Head of Financial</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Country Manager</span></a></li> -->

        

       <li class="header">USER</li>
        <li><a href="<?= site_url('admin/auth/logout'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Log out</span></a></li>
      </ul>


    </section>
    <!-- /.sidebar -->
  </aside>

  
<script>
  $("#<?= $cur_tab; ?>").addClass('active');
</script>

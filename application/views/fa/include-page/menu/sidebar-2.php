<?php 
$cur_tab = $this->uri->segment(2)==''?'dashboard': $this->uri->segment(2);  
?>  

  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less --> 
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?= base_url() ?>public/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <br>
          <p><?= ucwords($this->session->userdata('name')); ?></p>
          <!-- <a href="#"><i class="fa fa-circle text-success"></i> <?= ucwords($this->session->userdata('whse')); ?></a> -->
        </div>
      </div>
      
     

      <ul class="sidebar-menu">
      <li><a href="<?= base_url('admin/dashboard/dashboard_admin'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Dashboard By Request</span></a></li>
          <li><a href="<?= base_url('admin/dashboard/dashboard_service_center'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Dashboard By Service Center</span></a></li>
          <li><a href="<?= base_url('fa_con/fix_asset/fix_asset_management'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Fix Asset List</span></a></li>
          
          <!-- <li><a href="<?= base_url('fix-asset/item_asset/asset_expired'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Asset Expired</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Expire in  0-30 days</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Expire in 31-60 days</span></a></li>
          <li><a href="<?= base_url('#'); ?>"><i class="fa fa-circle-o text-red"></i> <span>Expire in 61-90 days</span></a></li> -->
      
        <li class="header">Transfer Request</li>
          <li><a href="<?= base_url('fa_con/request_new/transfer_request_status/1'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Draft</span></a></li>
          <li><a href="<?= base_url('fa_con/request_new/transfer_request_status/2'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Wait for Approve</span></a></li>
          <li><a href="<?= base_url('fa_con/request_new/transfer_request_status/3'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Approved</span></a></li>
          <li><a href="<?= base_url('fa_con/request_new/transfer_request_status/4'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Reject</span></a></li>
         
        
          <li class="header">Disposal Request</li>
          <li><a href="<?= base_url('fa_con/request_new/disposal_request_status/1'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Draft</span></a></li>
          <li><a href="<?= base_url('fa_con/request_new/disposal_request_status/2'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Wait for Approve</span></a></li>
          <li><a href="<?= base_url('fa_con/request_new/disposal_request_status/3'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Approved</span></a></li>
          <li><a href="<?= base_url('fa_con/request_new/disposal_request_status/4'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Reject</span></a></li>
       
   <li class="header">Master Data</li>
        <li id="fix_asset" class="treeview">
            <a href="#">
            <i class="fa fa-circle-o text-aqua"></i> <span>Fix Assets Management</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="view_users" class=""><a href="<?= base_url('fa_con/fix_asset/fix_asset_management'); ?>"><i class="fa fa-circle-o o text-yellow"></i> Fix Asset List</a></li>
			        <!-- <li id="user_group" class=""><a href="<?= base_url('fix-asset/asset_category/asset_category_management'); ?>"><i class="fa fa-circle-o text-yellow"></i> Assets Category</a></li> -->
              <!-- <li id="user_group" class=""><a href="<?= base_url('fix-asset/asset_type/asset_type_management'); ?>"><i class="fa fa-circle-o"></i> Assets Type</a></li> -->
              <!-- <li id="user_group" class=""><a href="<?= base_url('fix-asset/fix_asset/usage_management'); ?>"><i class="fa fa-circle-o"></i> Usage Status</a></li> -->
              <li><a href="<?= base_url('fa_con/dep_key/dep_key_management'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Dep Key</span></a></li>
              <li><a href="<?= base_url('fa_con/asset_class/asset_class_management'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Asset Class</span></a></li>
            </ul>
        </li>

       <li id="cost_center" class="treeview">
            <a href="#">
            <i class="fa fa-circle-o text-aqua"></i> <span>Cost Center Management</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
            <li><a href="<?= base_url('fa_con/costcenter/costcenter_management'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Cost Center</span></a></li>
        <li><a href="<?= base_url('fa_con/costcenter/country_management'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Country</span></a></li>
        <li><a href="<?= base_url('fa_con/costcenter/department_management'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Department</span></a></li>
        <li><a href="<?= base_url('fa_con/costcenter/location_management'); ?>"><i class="fa fa-circle-o text-yellow"></i> <span>Location</span></a></li>
            </ul>
        </li>

      
        <!-- <li><a href="<?= base_url('fa_con/dep_key/dep_key_management'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Dep Key</span></a></li> -->
        <!-- <li><a href="<?= base_url('fa_con/asset_class/asset_class_management'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Asset Class</span></a></li> -->
        <!-- <li><a href="<?= base_url('fix-asset/import_data/excel_import'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Import Excel Data</span></a></li> -->
       
        <li class="header">USER</li>
        <!-- <li id="users" class="treeview">
            <a href="#">
            <i class="fa fa-circle-o text-aqua"></i> <span>Users Management</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li id="add_user"><a href="<?= base_url('admin/users/add'); ?>"><i class="fa fa-circle-o"></i> Add User</a></li>
              <li id="view_users" class=""><a href="<?= base_url('admin/users'); ?>"><i class="fa fa-circle-o"></i> View Users</a></li>
            </ul>
        </li> -->
        <li id="view_users" class=""><a href="<?= base_url('fa_con/users/token_management'); ?>"><i class="fa fa-circle-o"></i> Line Token</a></li>
        <li id="view_users" class=""><a href="<?= base_url('fa_con/users/ci_users_management'); ?>"><i class="fa fa-circle-o"></i> Users Management</a></li>
        <li id="view_users" class=""><a href="<?= base_url('fa_con/users/ci_users_group'); ?>"><i class="fa fa-circle-o"></i> Role Management</a></li>
 
        <li><a href="<?= site_url('admin/auth/logout'); ?>"><i class="fa fa-circle-o text-aqua"></i> <span>Log out</span></a></li>
      </ul>


    </section>
    <!-- /.sidebar -->
  </aside>

  
<script>
  $("#<?= $cur_tab; ?>").addClass('active');
</script>

<!DOCTYPE html>
<html lang="en">
	<head>
		  <title><?=isset($title)?$title:'DHL-Fix Asset Management System' ?></title>
		  <!-- Tell the browser to be responsive to screen width -->
		  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		  <!-- Bootstrap 3.3.6 -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/bootstrap/css/bootstrap.min.css">
		  <!-- Font Awesome -->
		  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
		  <!-- Ionicons -->
		  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
		  <!-- Theme style -->
	      <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/AdminLTE.min.css">
	       <!-- Custom CSS -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/style.css">
		  <!-- AdminLTE Skins. Choose a skin from the css/skins. -->
		  <link rel="stylesheet" href="<?= base_url() ?>public/dist/css/skins/skin-blue.min.css">
		  <!-- jQuery 2.2.3 -->
		  <script src="<?= base_url() ?>public/plugins/jQuery/jquery-2.2.3.min.js"></script>
		  <!-- jQuery UI 1.11.4 -->
		  <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>

		
	</head>
	<body class="hold-transition skin-blue sidebar-mini">
		<div class="wrapper" style="height: auto; ">
			 <?php if($this->session->flashdata('msg') != ''): ?>
			    <div class="alert alert-warning flash-msg alert-dismissible">
			      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
			      <h4> Success!</h4>
			      <?= $this->session->flashdata('msg'); ?> 
			    </div>
			  <?php endif; ?> 
			
			<section id="container">
				<!--header start-->
				<header class="header white-bg">
                    <?php include('include-page/navbar-asset.php'); ?>
				</header>
				<!--header end-->
				<!--sidebar start-->
				<aside>

          <?php
		  include('include-page/sidebar-menu.php');
          ?>

				</aside>
				<!--sidebar end-->
				<!--main content start-->
				<section id="main-content">
					<div class="content-wrapper" style="min-height: 394px; padding:15px; background-image: url('<?= base_url('assets/images/whse.jpg'); ?>') ; no-repeat; background-size:cover;">
						<!-- page start-->
						 <!-- Content Header (Page header) -->
							<!-- <section class="content-header">
							<h1>
							</h1>
							<ol class="breadcrumb">
								<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
								<li><a href="#">Admin</a></li>
								<li class="active">Dashboard</li>
							</ol>
							</section> -->

    					<!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-md-12" style="color : white;" >
        <h1 style = ' padding : 1%;'>Dashboard Qty Asset By Service Center  </h1>

        <h1 style = ' padding : 1%;'>
          Total Assets all Service Center : <?php foreach($number_all_item as $row): ?>
                <?= $row['total_asset']; ?> 
            <?php endforeach; ?>
        </h1>
        <hr>
        </div>
    </div>

    <div class="row">
    <?php foreach($number_item_asset as $row): ?>
        <div class="col-md-4">
            <!-- small box -->
            <div class="small-box bg-red">
              <div class="inner">
              <h3 style = "font-size: 25px;" >
                
              <!-- Service Center <br> -->
              [ <?= $row['center_code']; ?> ]  <?= $row['service_center']; ?> 
              <br> 
                Assets : <?= $row['total_asset']; ?> 
                </h3>
              </div>
              <div class="icon">
                <i class="ion ion-stats-bars"></i>
              </div>
              <a href="<?= base_url("fa_con/fix_asset/fix_asset_management/");?><?= $row['center_code']; ?>  " class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
     
    </section>
    <!-- /.content -->


<script>
$("#examples").addClass('active');
$("#blank-page").addClass('active');
</script>
						<!-- page end-->
					</div>
				</section>
				<!--main content end-->
				<!--footer start-->
				<footer class="main-footer">
					<strong>Copyright © 2018 <a href="#">DHL</a></strong> All rights
					reserved.
				</footer>
				<!--footer end-->
			</section>

			<!-- /.control-sidebar -->
			<!-- <?php include('include/control_sidebar.php'); ?> -->

	</div>	
    
	
	<!-- Bootstrap 3.3.6 -->
	<script src="<?= base_url() ?>public/bootstrap/js/bootstrap.min.js"></script>
	<!-- AdminLTE App -->
	<script src="<?= base_url() ?>public/dist/js/app.min.js"></script>
	<!-- AdminLTE for demo purposes -->
	<script src="<?= base_url() ?>public/dist/js/demo.js"></script>
	<!-- page script -->
	<script type="text/javascript">
	  $(".flash-msg").fadeTo(2000, 500).slideUp(500, function(){
	    $(".flash-msg").slideUp(500);
	});
	</script>
	
	</body>
</html>
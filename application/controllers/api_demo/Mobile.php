<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Access-Control-Allow-Origin:*');

class Mobile extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('admin/Auth_model', 'Auth_model');
        $this->load->model('admin/Service_center_model', 'Service_center_model');
        $this->load->model('fa/Request_model', 'Request_model');
        $this->load->model('fa/Fix_asset_model', 'Fix_asset_model');
        // $this->load->model('admin/transaction_model', 'transaction_model');
    }

	public function index()
	{
		redirect(base_url('admin'));
    }

	public function testpost(){
		echo "Result-Post ";
		$username = $this->input->post('username');
        $password = $this->input->post('password');
		echo $username;
		echo " - ";
		echo $password;
		//var_dump($this->input->raw_input_stream);
	}
    
	public function testget(){
		echo "Result-Get ";
		$username = $this->input->get('username');
        $password = $this->input->get('password');
		echo $username;
		echo " - ";
		echo $password;
	}
    

    public function login(){

        $username = $this->input->get('username');
        $password = $this->input->get('password');
        
        //echo $_POST[0];
        //print_r($this->input->raw_input_stream);
        //print_r($_post);

        $data = array(
            'username' => $username,
            'password' => $password 
            );

			/*
            $result = $this->auth_model->login($data);
            if ($result == TRUE) {
                $admin_data = array(
                    'admin_id' => $result['id'],
                     'name' => $result['username'],
                     // add warehouse and user profile set session
                     'whse' => $result['center_code'],
                     'email' => $result['email'],
                     'firstname' => $result['firstname'],
                     'lastname' => $result['lastname'],
                     'role' => $result['role']
                    //  'is_admin_login' => TRUE
                );

                echo json_encode($admin_data);
            }
            else{
                $msg = 'Invalid Email or Password!!!';
                echo $msg;
            }
			*/
			$result = $this->Auth_model->login($data);
			  $admin_data = array(
                    'admin_id' => $result['id'],
                     'name' => $result['username'],
                     // add warehouse and user profile set session
                     'whse' => $result['center_code'],
                     'email' => $result['email'],
                     'firstname' => $result['firstname'],
                     'lastname' => $result['lastname'],
                     'role' => $result['role']
                    //  'is_admin_login' => TRUE
                );

                echo json_encode($admin_data);
    }

    public function getcostcenter(){
	    // $item_barcode = $this->input->get('barcode_text');
		// $whse_center_code = $this->input->get('whse');
    
        // $data = array(
        //     'item_barcode' => $item_barcode,
        //     'whse_center_code' => $whse_center_code 
        //     );
	
				$result = $this->Service_center_model->get_active_costcenter();
	
                echo json_encode($result);
    }


	public function getitem(){
	    $item_barcode = $this->input->get('barcode_text');
		$whse_center_code = $this->input->get('whse');
    
        $data = array(
            'item_barcode' => $item_barcode,
            'whse_center_code' => $whse_center_code 
            );
	
				$result = $this->Item_model->get_item_by_whse_barcode($data);
	
                echo json_encode($result);
    }

	public function getstock(){
	    //$barcode = $this->input->get('barcode_text');
		$whse_center_code = $this->input->get('center_code');
    
        $data = array(
            //'item_barcode' => $item_barcode,
            'whse_center_code' => $whse_center_code 
            );
				$result = $this->Item_model->get_stock_onhand_by_whse($whse_center_code);		
				$res =  json_encode($result);
                echo $res;
    }

	public function get_transaction(){
	    //$barcode = $this->input->get('barcode_text');
			$username = $this->input->get('username');
			$center_code = $this->input->get('center_code');
		
			$data = array(
			'username' => $username,
            'center_code' => $center_code
            );
				$result = $this->transaction_model->get_transaction_by_user($data);		
				$res =  json_encode($result);
                echo $res;

					
    }

    public function get_request_list_by_user(){
	    //$barcode = $this->input->get('barcode_text');
			$user_id = $this->input->get('user_id');
			// $center_code = $this->input->get('center_code');
		
			// $data = array(
			// 'user_request' => $user_id
            // // 'center_code' => $center_code
            // );
				$result = $this->Request_model->get_request_by_user($user_id);		
				$res =  json_encode($result);
                echo $res;

					
    }
    
    public function get_approve_list_by_req_num(){
	    //$barcode = $this->input->get('barcode_text');
			$req_no = $this->input->get('req_num');
			
				$result = $this->Request_model->get_approve_list($req_no);	
				$res =  json_encode($result);
                echo $res;

					
    }

    public function get_asset_list_by_req_num(){
	    //$barcode = $this->input->get('barcode_text');
			    $req_num = $this->input->get('req_num');
			
				$result = $this->Request_model->get_asset_list_by_req_num($req_num);		
				$res =  json_encode($result);
                echo $res;

					
    }

    public function delete_asset_from_list(){
	  
			    $request_detail_id = $this->input->get('request_detail_id');
			
				$result_model = $this->Request_model->delete_asset_from_list($request_detail_id);		
                
            //     if($result_model){
			
                    // $array = array('code'=> 'Success', 'msg'=> $request_detail_id.' Delete successfully!');
                    $array = array('code'=> 'Success', 'msg'=> 'Delete successfully!');
                    $result = json_encode($array);
    
                    echo $result;
            //     //echo "Issue Successfully!";
            // }
            // else {
            //         $array = array('code'=> 'Error', 'msg'=> 'Can not delete asset');
            //         $result = json_encode($array);
    
                    // echo $result_model;
            // }   
               				
    }

    public function get_asset_detail(){

        $id = $this->input->get('asset_id');

        $result = $this->Fix_asset_model->get_item_by_id($id);		
		$res =  json_encode($result);
        echo $res;
    }

    public function submit_request(){
	  
        $req_number = $this->input->get('req_number');
    
        // $submit_status = '2';

                
        $result_model = $this->Request_model->submit_request($req_number);	

        if($result_model == true){
            
            $result_list = $this->Request_model->insert_approve_list($req_number);
            
            if($result_list == true){

                $array = array('code'=> 'Success', 'msg'=> 'Submit request successfully!');
                $result = json_encode($array);
        
                // curl
                        // Get cURL resource
                            $curl = curl_init();
                            // Set some options - we are passing in a useragent too here
                            curl_setopt_array($curl, array(
                                CURLOPT_RETURNTRANSFER => 1,
                                CURLOPT_URL => 'https://devdeethailand.com/dhl-fa-v3/api/send_email/alert_approver/'.$req_number.'',
                                CURLOPT_USERAGENT => 'Codular Sample cURL Request'
                            ));
                            // Send the request & save response to $resp
                            $resp = curl_exec($curl);
                            // Close request to clear up some resources
                            curl_close($curl);
                            
                            // echo $curl;
                            // echo $resp;
                // end of curl

                echo $result;

            } else {
                $array = array('code'=> 'Warning', 'msg'=> 'Request status is already submit!');
                $result = json_encode($array);
        
                echo $result;
                // $next = base_url("api/send_email/alert_approver/$req_number");
                // echo $next;               
            }

        }else {
            $array = array('code'=> 'Warning', 'msg'=> 'can not create approve list !');
            $result = json_encode($array);
    
            echo $result;
        }

       
        
                       
    }

    public function delete_request(){
	  
        $req_number = $this->input->get('req_number');
    
        // $submit_status = '2';

                
        $result_detail = $this->Request_model->delete_request_detail($req_number);	

        $result_header = $this->Request_model->delete_request_header($req_number);	

        $array = array('code'=> 'Warning', 'msg'=> 'Delete request complete !');
        $result = json_encode($array);
       
        
                       
    }
    

    public function create_new_request(){
		
		// get parameter from mobile
		$request_type = $this->input->get('request_type');
		$to_costcenter = $this->input->get('to_costcenter');
		$center_code =  $this->input->get('center_code');
        $username = $this->input->get('username');
        $user_id = $this->input->get('user_id');

        $ref_uniqid = uniqid(); 
        
        $submit_status = '1';


        $data = array(
            'req_type' => $request_type,
            'to_costcenter' => $to_costcenter,
            'submit_status ' => $submit_status,
            //'center_code' => $center_code,
            'user_request' => $user_id,
            'ref_uniqid' => $ref_uniqid
            );


        // print_r($data);
        // insert t_transaction    
        $result = $this->Request_model->insert_request_header($data);
        
        if($result){
			
                $item = $this->Request_model->get_request_by_uniqid($ref_uniqid);
                
                $req_number = $item[0]['req_number'];

				$array = array('code'=> 'Success', 'msg'=> $req_number.' create successfully!');
				$result = json_encode($array);

                echo $result;
            //echo "Issue Successfully!";
        }
        else {
				$array = array('code'=> 'Error', 'msg'=> 'Can not save data');
				$result = json_encode($array);

                echo $result;
        }   
    } 

    public function add_asset_to_request(){

        $id = $this->input->get('user_id');
        $reqest_no = $this->input->get('req_number');
        $qr_code_id = $this->input->get('qr_code');

        // check it asset 
        $it_asset_detail = $this->Fix_asset_model->check_it_asset($qr_code_id);
 
        if ($it_asset_detail == false) {
            $asset_check = 'n' ;
        } else {
            $asset_check = 'y' ;
            $it_asset = $it_asset_detail[0]['asset_id'];
            $asset_location = $it_asset_detail[0]['service_center'];
        }

        // check user role
        $user_detail = $this->Auth_model->get_user_role($id);
       
        if ($user_detail == false) {
            $user_role = '';
        } else {
            $user_role = $user_detail[0]['role'];
        }


        // check user location 
        $user_loc_detail = $this->Auth_model->get_location($id);

        if ($user_loc_detail == false) {
            $user_loc = '';
        } else {
            $user_loc = $user_loc_detail[0]['sc_id'];
        }

        // check duplicate asset in request
        $req_asset_detail = $this->Request_model->get_duplicate_asset_in_request($reqest_no, $it_asset);
        if ($req_asset_detail == false) {
           $duplicated = 'n';
        } else {
            $duplicated = 'y';
        }
       

        $data = array(
                    'user_id' => $id,
                    'request_no' => $reqest_no,
                    'qr_code' => $qr_code_id,
                    // 'it_asset_check' => $it_asset,
                    'user_role' => $user_role
                    );

        // print_r($data) ;

        if ($asset_check == 'n'){
            $array = array('code'=> 'Warning', 'msg'=> 'Not found Asset !');
            $result = json_encode($array);
            echo $result;
        } else if($user_role == 2 and $it_asset == 1){
            $array = array('code'=> 'Warning', 'msg'=> 'You are location admin this asset is IT Asset !');
            $result = json_encode($array);
            echo $result;
        } else if ($user_role == 2 and $asset_location !== $user_loc) {
            $array = array('code'=> 'Warning', 'msg'=> 'Asset is '.$asset_location.' your service center is '.$user_loc);
            $result = json_encode($array);
            echo $result;
        } else if($user_role == 4 and $it_asset == 0){
            $array = array('code'=> 'Warning', 'msg'=> $reqest_no.'You are IT admin this asset is not IT Asset !');
            $result = json_encode($array);
            echo $result;
        }else if ($duplicated == 'y'){
            $array = array('code'=> 'Warning', 'msg'=> 'Asset is already exits in this request !');
            $result = json_encode($array);
            echo $result;
        } else {
            // Passed check all condition start to insert
            // function for find asset_id and book_value
            $asset_detail = $this->Fix_asset_model->get_item_asset_detail($qr_code_id);

            $asset_id = $asset_detail[0]['asset_id'];
            $net_value = $asset_detail[0]['book_val'];
            $data = array(
                // 'user_id' => $id,
                'request_no' => $reqest_no,
                // 'qr_code' => $qr_code_id,
                'asset_id' => $asset_id,
                'net_value' => $net_value
                );
            
            $result = $this->Request_model->insert_request_detail($data);

            $array = array('code'=> 'Success', 'msg'=> 'Add asset to request no. '.$reqest_no.' successfully!');
            $result = json_encode($array);
            echo $result;
        }

        
    }

    public function qtyissue(){
		
		// get parameter from mobile
		$item_id = $this->input->get('item_id');
		$qty = $this->input->get('qty');
        $qty_neg = $qty*-1;
		$center_code =  $this->input->get('center_code');
		$username = $this->input->get('username');

		$note = $this->input->get('note');
		$ref_doc = $this->input->get('ref_doc');

        // set default value for field
        $transaction_type = "i";
        $type_reason = '1';
        $unit = '1';
        $qty_convert = '1';

		if ($note==1) {
			$note = "To Customer";
		} elseif ($note==2) {
			$note = "To Spare";
		} elseif (strlen($ref_doc) > 0) {
			$note = "To Customer";
		} else {
            $note = "To Spare";
        }


		//$note = "SCM Mobile App";

        $data = array(
            'item_id' => $item_id,
            'transaction_type' => $transaction_type,
            'type_reason' => $type_reason,
            'qty' => $qty_neg,
            'unit' => $unit,
            'qty_convert' => $qty_convert,
			'note' => $note,
            'center_code' => $center_code,
            'username' => $username ,
			'note' => $note,
			'ref_document' => $ref_doc 
            );

        // insert t_transaction    
        $result = $this->request_model->insert_request_header($data);
        if($result){
			
			$result = $this->Item_model->get_stock_onhand_by_whse($center_code);		
				// update stock onhand
				$sql = "UPDATE t_item_whse as t1
				JOIN v_item_on_hand as t2 ON t1.item_code = t2.item_id and t1.id_center = t2.id_center
				SET t1.qty_on_hand = t2.qty_balance ;
				";
				$query = $this->db->query($sql);

				$array = array('code'=> 'Success', 'msg'=> 'Issue save Successfully!');
				$result = json_encode($array);

                echo $result;
            //echo "Issue Successfully!";
        }
        else {
				$array = array('code'=> 'Error', 'msg'=> 'Can not save data');
				$result = json_encode($array);

                echo $result;
        }   
    }   
}

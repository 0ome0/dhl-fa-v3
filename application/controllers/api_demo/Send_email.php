<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Send_email extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

        $this->load->library('grocery_CRUD');
        // $this->load->model('admin/auth_model', 'auth_model');
        $this->load->library('email');

        $this->load->model('admin/Auth_model', 'Auth_model');
        $this->load->model('admin/Service_center_model', 'Service_center_model');
        $this->load->model('fa/Request_model', 'Request_model');
        $this->load->model('fa/Fix_asset_model', 'Fix_asset_model');
	}


    public function alert_approver(){

            // $req_no = $this->uri->segment(4);
            // prepare data
            $email = "";
            $data['req_total_value'] = "";
            // end prepare data



            $req_no = $this->uri->segment(4);

            $data['req_header_description'] =  $this->Request_model->get_request_header_description($req_no);
            $data['req_total_value'] =  $this->Request_model->request_total_book_value($req_no);

            $req_num = $req_no;
            $data['req_detail'] =  $this->Request_model->get_asset_list_by_req_num($req_num);

            
            $current_approver =  $this->Request_model->current_approver($req_no);
            if($current_approver==false){
                // not found current approver
                // echo " not found current approver";
               
            } else{
                $item = $this->Request_model->current_approver($req_no);
                $email = $item[0]['email'];
                $from_approver = $item[0]['user_approve_id'];

                $data['current_approver'] =  $this->Request_model->current_approver($req_no);
                $data['approve_list'] =  $this->Request_model->get_approve_list($req_no);



                $approved = base_url("api/send_email/approve_request/2/$from_approver/$req_no");
                $rejected = base_url("api/send_email/approve_request/3/$from_approver/$req_no");

                $data['link_approved'] =  $approved;
                $data['link_rejected'] =  $rejected;

                // print_r($data);
                // echo $email;
                            // send email
                            $config = Array(
                                'protocol'  => 'smtp',
                                'smtp_host' => 'mail.devdeethailand.com',
                                'smtp_port' =>  25,
                                'smtp_user' => 'fixasset@devdeethailand.com',
                                'smtp_pass' => '123456',
                                'mailtype'  => 'html',
                                'charset'   => 'utf-8',
                                'smtp_crypto' => "tls"
                            );
    
                            $this->load->library('email', $config);
                            $this->email->set_newline("\r\n");
                            $this->email->set_mailtype("html");
                            $this->email->from('fixasset@devdeethailand.com', 'Fix Asset Management System');
                   
                            $this->email->to($email);
                            $this->email->subject('Request No. '.$req_no.' waiting for approve');
                            
                            $body = $this->load->view('emails/alert_approver.php',$data,TRUE);
                            
                            $this->email->message($body); 

                                    if (!$this->email->send())
                                    {
                                        // show_error($this->email->print_debugger());
                                    }
                                    else
                                    {
                                        // echo true;
                                    }

            }
                       
           
    } // end alert_approver function


    public function approve_request(){
        
        $approve_status = $this->uri->segment(4);
        $from_approver = $this->uri->segment(5);
        $req_no = $this->uri->segment(6);
        
        date_default_timezone_set("Asia/Bangkok");
		$approve_date = date("Y-m-d H:i:s");

        // echo  $approve_status;
        // echo  $from_approver;
        // echo  $req_no;
        // echo  $approve_date;

        if($approve_status == 3){
            // reject request 
            // 1.update request status -> reject (3)
                $result =  $this->Request_model->update_request_status($req_no,$from_approver,$approve_status,$approve_date);
                $result_req_header =  $this->Request_model->update_request_status_header($req_no,$approve_status);
                // echo "Rejected";
            // 2.alert user request and account admin -> complete workflow 
            $item = $this->Request_model->get_request_header_description($req_no);
            $email_requestor = $item[0]['email']; 

            $item_email = $this->Request_model->get_account_manager_email();
            $email_account_manager = $item_email[0]['email']; 


            $data['req_header_description'] =  $this->Request_model->get_request_header_description($req_no);
            $data['req_total_value'] =  $this->Request_model->request_total_book_value($req_no);

            $req_num = $req_no;
            $data['req_detail'] =  $this->Request_model->get_asset_list_by_req_num($req_num);

            $data['current_approver'] =  $this->Request_model->current_approver($req_no);
            $data['approve_list'] =  $this->Request_model->get_approve_list($req_no);
            
            // echo $email_requestor;
            // echo $email_account_manager;

                    $config = Array(
                        'protocol'  => 'smtp',
                        'smtp_host' => 'mail.devdeethailand.com',
                        'smtp_port' =>  25,
                        'smtp_user' => 'fixasset@devdeethailand.com',
                        'smtp_pass' => '123456',
                        'mailtype'  => 'html',
                        'charset'   => 'utf-8',
                        'smtp_crypto' => "tls"
                    );

                    $this->load->library('email', $config);
                    $this->email->set_newline("\r\n");
                    $this->email->set_mailtype("html");
                    $this->email->from('fixasset@devdeethailand.com', 'Fix Asset Management System');
        
                    $this->email->to($email_requestor,$email_account_manager);
                    $this->email->subject('Request No. '.$req_no.' has been reject');
                    
                    $body = $this->load->view('emails/request_rejected.php',$data,TRUE);
                    
                    $this->email->message($body); 

                            if (!$this->email->send())
                            {
                                // show_error($this->email->print_debugger());
                            }
                            else
                            {
                                // echo true;
                            }
                    echo "Request No. $req_no has been reject";
                    $this->alert_line_notify($req_no,$approve_status);

        }else if($approve_status == 2){
                
                // appreved request
                // 1.update request status -> approve (2)
                $result =  $this->Request_model->update_request_status($req_no,$from_approver,$approve_status,$approve_date);
              
            
                // 2.find next level approve and send email 
                $current_approver =  $this->Request_model->current_approver($req_no);
                if($current_approver==false){
                    // not found current approver = Complete workflow send email to user 
                    // echo " not found current approver = Complete workflow";
                    $result_req_header =  $this->Request_model->update_request_status_header($req_no,$approve_status);
                    $this->request_completed($req_no);
                    

                } else{ 
                    // send email alert for next approver
                    // curl
                        // Get cURL resource
                        $curl = curl_init();
                        // Set some options - we are passing in a useragent too here
                        $req_number = $req_no;
                        curl_setopt_array($curl, array(
                            CURLOPT_RETURNTRANSFER => 1,
                            CURLOPT_URL => 'https://devdeethailand.com/dhl-fa-v3/api/send_email/alert_approver/'.$req_number.'',
                            CURLOPT_USERAGENT => 'Codular Sample cURL Request'
                        ));
                        // Send the request & save response to $resp
                        $resp = curl_exec($curl);
                        // Close request to clear up some resources
                        curl_close($curl);
                        
                        // echo $curl;
                        echo $resp;
            // end of curl
                }
                echo "Approved";
        } else {
            // error case status <> 1,2
                echo "Error";
        }

    }

    public function request_completed($req_no){

        // get request information 
        $item = $this->Request_model->get_request_header_description($req_no);
        
        // 1. update fix asset status and insert log table
            
        // 1.1  check transfer or disposal
            $req_type = $item[0]['req_type'];   
            
            // 1.2 insert history log table
            $this->Request_model->insert_history_log($req_no);

            if($req_type == '1'){ //  1 = transfer

                // 1.2.1 update costcenter in t_fa
                $this->Request_model->update_asset_type_transfer($req_no);

            }else if($req_type == '2'){ // 2 = disposal

                // 1.2.2 update disposal flag in t_fa
                $this->Request_model->update_asset_type_disposal($req_no);

            }
            
        // 2. send email
         // 2.alert user request and account admin -> complete workflow 
         
         $email_requestor = $item[0]['email']; 

         $item_email = $this->Request_model->get_account_manager_email();
         $email_account_manager = $item_email[0]['email']; 


         $data['req_header_description'] =  $this->Request_model->get_request_header_description($req_no);
         $data['req_total_value'] =  $this->Request_model->request_total_book_value($req_no);

         $req_num = $req_no;
         $data['req_detail'] =  $this->Request_model->get_asset_list_by_req_num($req_num);

         $data['current_approver'] =  $this->Request_model->current_approver($req_no);
         $data['approve_list'] =  $this->Request_model->get_approve_list($req_no);
         
         // echo $email_requestor;
         // echo $email_account_manager;

                 $config = Array(
                     'protocol'  => 'smtp',
                     'smtp_host' => 'mail.devdeethailand.com',
                     'smtp_port' =>  25,
                     'smtp_user' => 'fixasset@devdeethailand.com',
                     'smtp_pass' => '123456',
                     'mailtype'  => 'html',
                     'charset'   => 'utf-8',
                     'smtp_crypto' => "tls"
                 );

                 $this->load->library('email', $config);
                 $this->email->set_newline("\r\n");
                 $this->email->set_mailtype("html");
                 $this->email->from('fixasset@devdeethailand.com', 'Fix Asset Management System');
     
                 $this->email->to($email_requestor,$email_account_manager);
                 $this->email->subject('ํYour request No. '.$req_no.' has been completely approved.');
                 
                 $body = $this->load->view('emails/request_completed.php',$data,TRUE);
                 
                 $this->email->message($body); 

                         if (!$this->email->send())
                         {
                             // show_error($this->email->print_debugger());
                         }
                         else
                         {
                             // echo true;
                         }
                //  echo "Request No. '.$req_no.' been completely approved.";
                $approve_status = "2";
                $this->alert_line_notify($req_no,$approve_status);

    }

    public function alert_line_notify($req_no,$approve_status){

        $action = "wait for approve";

        if($approve_status == 2){
            $action = "approved";
        } else if($approve_status == 3){
            $action = "rejected";
        }


        $item_token = $this->Auth_model->get_token_key();
        $Token = $item_token[0]['token_key'];

        if($item_token){
            $Token = $item_token[0]['token_key'];
        }else {
            // default key
            $Token = 'SNFr4r1kfMwW8gI9CKFLqPjL2qr9IlCDWltY81d8rYB';
        }
        

        date_default_timezone_set("Asia/Bangkok");
        $date = date("Y-m-d H:i:s");
        
        // call line api
        $message = "DHL-Fix Asset alert request document no. $req_no has been $action / Date : $date";

        // echo $message;

        $lineapi = $Token; // ใส่ token key ที่ได้มา
        $mms =  trim($message); // ข้อความที่ต้องการส่ง
        date_default_timezone_set("Asia/Bangkok");
        $chOne = curl_init(); 
        curl_setopt( $chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify"); 
        // SSL USE 
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYHOST, 0); 
        curl_setopt( $chOne, CURLOPT_SSL_VERIFYPEER, 0); 
        //POST 
        curl_setopt( $chOne, CURLOPT_POST, 1); 
        curl_setopt( $chOne, CURLOPT_POSTFIELDS, "message=$mms"); 
        curl_setopt( $chOne, CURLOPT_FOLLOWLOCATION, 1); 
        $headers = array( 'Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer '.$lineapi.'', );
            curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers); 
        curl_setopt( $chOne, CURLOPT_RETURNTRANSFER, 1); 
        $result = curl_exec( $chOne ); 
        //Check error 
        if(curl_error($chOne)) 
        { 
            // echo 'error:' . curl_error($chOne); 
        } 
        else { 
        $result_ = json_decode($result, true); 
        // echo "status : ".$result_['status']; echo "message : ". $result_['message'];
        // redirect('/menu_list/ordered','refresh');
            } 
        curl_close( $chOne );

    }

} // end of controller 
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
		// $this->load->model('itasset/Vendor_model', 'Vendor_model');
	}

	// public function _example_output($output = null)
	// {
	// 	$this->load->view('fix-asset/layout-asset_type.php',(array)$output);
	// }

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function ci_users_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
            $crud->set_table('ci_users');
            $crud->where('role > ','1');
			$crud->set_relation('role','ci_user_groups','group_name',null,'id desc');
			$crud->display_as('role','User Role');
			$crud->set_subject('User Management');

            // $crud->required_fields('dep_key_id');
            // $crud->required_fields('year');
            // $crud->required_fields('percentage');
            // $crud->required_fields('status');
            // $crud->hidden_fields('created_date');
            // $crud->field_type('created_date', 'hidden', 3);


			// $crud->columns('dep_key_id','year','percentage');

			// $crud->set_field_upload('center_map','assets/uploads/files');
			// // $crud->field_type('center_code','readonly');
			
			// insert new user
			$crud->callback_before_insert(array($this,'add_new_user'));
			$crud->callback_before_update(array($this,'add_new_user'));
			
			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('fa/users.php',(array)$output);
	}

	public function ci_users_group()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
            $crud->set_table('ci_user_groups');
            $crud->where('id > ','2');
			// $crud->set_relation('role','ci_user_groups','group_name',null,'id desc');
			$crud->display_as('group_name','User Group');
			$crud->display_as('approve_mix','Approve Min Amount');
			$crud->display_as('approve_max','Approve Min Amount');
			$crud->set_subject('User Level');
			

            // $crud->required_fields('dep_key_id');
            // $crud->required_fields('year');
            // $crud->required_fields('percentage');
            // $crud->required_fields('status');
            // $crud->hidden_fields('created_date');
			$crud->field_type('id', 'hidden', 3);
			
			
			$crud->field_type('group_name','readonly');
			$crud->field_type('approve_amount','integer');


			$crud->columns('group_name','approve_min','approve','request');
			$crud->unset_add();
			$crud->unset_delete();

			// $crud->set_field_upload('center_map','assets/uploads/files');
			// // $crud->field_type('center_code','readonly');

			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('fa/users.php',(array)$output);
	}

	public function assign_approver(){
		$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
            $crud->set_table('ci_user_groups');
			$crud->where('ci_user_groups.id > ','5');
			
			// role = 2 as location admin
			$crud->set_relation('user_approve_id','ci_users','{username} - {firstname} {lastname} ',array('ci_users.role >' => '5'),'firstname asc');
			$crud->display_as('ci_user_groups.id','User Approver');

			$crud->display_as('approve_min','Approve Min Amount');
			$crud->set_subject('User Level');
			

            // $crud->required_fields('dep_key_id');
            // $crud->required_fields('year');
            // $crud->required_fields('percentage');
            // $crud->required_fields('status');
            // $crud->hidden_fields('created_date');
			$crud->field_type('ci_user_groups.id', 'hidden', 3);
			
			
			
			// $crud->field_type('approve_amount','integer');


			$crud->columns('group_name','user_approve_id');
			$crud->unset_add();
			$crud->unset_delete();

			$crud->edit_fields('group_name','user_approve_id');
			$crud->field_type('group_name','readonly');

			// $crud->set_field_upload('center_map','assets/uploads/files');
			// // $crud->field_type('center_code','readonly');

			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('fa/users.php',(array)$output);
	}

	public function token_management(){
		$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
            $crud->set_table('t_token');
			$crud->display_as('token_key','Token Key Line Notify');
			$crud->set_subject('Token Key Line Notify');
		
			$crud->unset_add();
			$crud->unset_delete();

		
			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('fa/token_view.php',(array)$output);
	}


	// callback function hasing password

	public function add_new_user($post_array){

		$post_array['password'] = password_hash($post_array['password'] , PASSWORD_BCRYPT);
		return $post_array;

	}

	public function change_password($post_array){

		$post_array['password'] = password_hash($post_array['password'] , PASSWORD_BCRYPT);
		return $post_array;
	}

}

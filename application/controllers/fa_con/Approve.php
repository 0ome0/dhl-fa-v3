<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Approve extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
		$this->load->model('fa/Date_model', 'Date_model');
		$this->load->model('fa/Request_model', 'Request_model');
		


	}

	// public function _example_output($output = null)
	// {
	// 	$this->load->view('fix-asset/layout-asset_type.php',(array)$output);
	// }

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}


	public function wait_for_approve(){
            

        
            $approve_by_role = $this->uri->segment(4);
            $role_id = $this->uri->segment(5);
            $user_id = $this->uri->segment(6);



			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_approve_level');			

			$crud->set_relation('approve_status','t_approve_status','approve_status_description',null);
			$crud->display_as('approve_status_description','Approve Status');

			$crud->set_primary_key('req_number','v_request_status');
			$crud->set_relation('req_number','v_request_status','req_status',null);
			$crud->display_as('req_status','Request Status');
			$crud->where('req_status =','1');	// wait for approve

			// $crud->set_relation('req_type','t_request_type','type_name',null,'type_id asc');
			// $crud->display_as('req_type','Request for');

			// $crud->set_relation('user_request','ci_users','username',null);
			// $crud->display_as('user_request','User Request');

			// $crud->set_primary_key('req_number','v_request_status');
			// $crud->set_relation('req_number','v_request_status','req_number',null);
			// $crud->display_as('req_status','Request Status');
	
			// ****** Show Draft Status only
			$crud->where('submit_status =','2');	
            $crud->where('approve_status =','1');	
            

            switch ($approve_by_role) {
                case "0": // where user_id
                $crud->where('user_approver =',$user_id);
                    break;
                case "1": // where role_id
                $crud->where('role_id =',$role_id);
                // $crud->where('user =',$role_id);
                    break;

            }


			// ****** list asset in own location only
            // change to query v_asset_for_request
            // $crud->set_relation('asset_id','t_fa','{asset_no}-{asset_description}-{invent_no}',null,'asset_id asc');
            // $crud->display_as('asset_id','Fix Asset');
            
             // ****** list locatin is not Costcenter asset current
             // change to query v_location
            //  $crud->set_relation('to_costcenter','t_costcenter','{costcenter_id}',null,'costcenter_id asc');
			//  $crud->display_as('costcenter_id','To Costcenter');
			// $crud->set_primary_key('costcenter_id','v_costcenter_mapping');
			// $crud->set_relation('to_costcenter','v_costcenter_mapping','{costcenter_id} {function_description}-{function_name}-{sc_name}',null,'costcenter_id asc');
			// $crud->display_as('costcenter_id','To Costcenter');
			

            // $crud->set_relation('submit_status','t_submit_status','submit_description',null,'submit_id asc');
			// $crud->display_as('submit_id','Submit Status');

			// $crud->display_as('req_number','Request No.');
			

			$crud->set_subject('Wait for Approve');

			// $crud->columns('request_date','req_number','req_type','asset_id','to_costcenter','submit_status','user_request');
			// $crud->order_by('request_date','desc');


			$crud->add_action('Approve List', ''.base_url().'assets/images/log.png', 'fa_con/request/approve_list/v',$action_target = 'log_icon');


			$crud->unset_add();
			// $crud->unset_edit();
			$crud->unset_delete();
			
			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('fa/wait_for_approve.php',(array)$output);
	}

	public function request_approved(){
			
		
		$user_id = $this->session->userdata('user_id');

		$crud = new grocery_CRUD();

		$crud->set_theme('flexigrid');
		$crud->set_table('t_request');			

		$crud->set_relation('req_type','t_request_type','type_name',null,'type_id asc');
		$crud->display_as('req_type','Request for');

		$crud->set_primary_key('req_number','v_request_status');
		$crud->set_relation('req_number','v_request_status','req_number',null);
		$crud->display_as('req_status','Request Status');

		$crud->set_relation('user_request','ci_users','username',null);
		$crud->display_as('user_request','User Request');

		// ****** Show Draft Status only
		$crud->where('submit_status =','2');	
		$crud->where('req_status =','2');	
		$crud->where('user_request =',$user_id);

		// ****** list asset in own location only
		// change to query v_asset_for_request
		$crud->set_relation('asset_id','t_fa','{asset_no}-{asset_description}-{invent_no}',null,'asset_id asc');
		$crud->display_as('asset_id','Fix Asset');
		
		 // ****** list locatin is not Costcenter asset current
		 // change to query v_location
		//  $crud->set_relation('to_costcenter','t_costcenter','{costcenter_id}',null,'costcenter_id asc');
		//  $crud->display_as('costcenter_id','To Costcenter');
		$crud->set_primary_key('costcenter_id','v_costcenter_mapping');
		$crud->set_relation('to_costcenter','v_costcenter_mapping','{costcenter_id} {function_description}-{function_name}-{sc_name}',null,'costcenter_id asc');
		$crud->display_as('costcenter_id','To Costcenter');
		

		$crud->set_relation('submit_status','t_submit_status','submit_description',null,'submit_id asc');
		$crud->display_as('submit_id','Submit Status');

		$crud->display_as('req_number','Request No.');
		

		$crud->set_subject('Request Approved');

		$crud->columns('request_date','req_number','req_type','asset_id','to_costcenter','submit_status','user_request');
		$crud->order_by('request_date','desc');


		$crud->add_action('Approve List', ''.base_url().'assets/images/log.png', 'fa_con/request/approve_list/v',$action_target = 'log_icon');


		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();
		
		$output = $crud->render();

		// $this->_example_output($output);
		$this->load->view('fa/request_approved.php',(array)$output);
	}

	public function request_reject(){
			
		$user_id = $this->session->userdata('user_id');

		$crud = new grocery_CRUD();

		$crud->set_theme('flexigrid');
		$crud->set_table('t_request');			

		$crud->set_relation('req_type','t_request_type','type_name',null,'type_id asc');
		$crud->display_as('req_type','Request for');

		$crud->set_primary_key('req_number','v_request_status');
		$crud->set_relation('req_number','v_request_status','req_number',null);
		$crud->display_as('req_status','Request Status');

		// $crud->set_relation('v_request_status.approve_status','t_approve_status','approve_status_description',null);
		// $crud->display_as('approve_status_description','Approve Status');

		$crud->set_relation('user_request','ci_users','username',null);
		$crud->display_as('user_request','User Request');

		// ****** Show Draft Status only
		$crud->where('submit_status =','2');	
		$crud->where('req_status =','3');	
		$crud->where('user_request =',$user_id);

		// ****** list asset in own location only
		// change to query v_asset_for_request
		$crud->set_relation('asset_id','t_fa','{asset_no}-{asset_description}-{invent_no}',null,'asset_id asc');
		$crud->display_as('asset_id','Fix Asset');
		
		 // ****** list locatin is not Costcenter asset current
		 // change to query v_location
		//  $crud->set_relation('to_costcenter','t_costcenter','{costcenter_id}',null,'costcenter_id asc');
		//  $crud->display_as('costcenter_id','To Costcenter');
		$crud->set_primary_key('costcenter_id','v_costcenter_mapping');
		$crud->set_relation('to_costcenter','v_costcenter_mapping','{costcenter_id} {function_description}-{function_name}-{sc_name}',null,'costcenter_id asc');
		$crud->display_as('costcenter_id','To Costcenter');
		

		$crud->set_relation('submit_status','t_submit_status','submit_description',null,'submit_id asc');
		$crud->display_as('submit_id','Submit Status');

		$crud->display_as('req_number','Request No.');
		

		$crud->set_subject('Request Reject');

		$crud->columns('request_date','req_number','req_type','asset_id','to_costcenter','submit_status','user_request');
		$crud->order_by('request_date','desc');


		$crud->add_action('Approve List', ''.base_url().'assets/images/log.png', 'fa_con/request/approve_list/v',$action_target = 'log_icon');


		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();
		
		$output = $crud->render();

		// $this->_example_output($output);
		$this->load->view('fa/request_reject.php',(array)$output);
	}

	// public function approve_list(){
			
	// 		$req_number = $this->uri->segment(4);

	// 		$crud = new grocery_CRUD();

	// 		$crud->set_theme('flexigrid');
	// 		$crud->set_table('v_role_approve_user');	
	// 		$crud->set_primary_key('trans_id','v_role_approve_user');
	// 		$crud->where('req_number =', $req_number);	

	// 		$crud->set_relation('role_id','ci_user_groups','group_name',null);
	// 		$crud->display_as('group_name','Role');

	// 		$crud->display_as('role_id','Role');

	// 		$crud->set_relation('approve_status','t_approve_status','approve_status_description',null);
	// 		$crud->display_as('approve_status_description','Approve Status');

	// 		$crud->set_relation('user_approver','ci_users','{firstname} {lastname} [{username}]',null);
	// 		$crud->display_as('username','User Approver');

	// 		$crud->display_as('req_number','Request No.');
	// 		$crud->display_as('it_approve','Need IT Approve');

	// 		$crud->columns('req_number','review','approve_status','role_id','user_approver','approve_min','approve_max','book_val','it_approve','asset_no','asset_description','costcenter','to_costcenter');
	// 		$crud->order_by('role_id','asc');

	// 		$crud->unset_operations();
	// 		$crud->unset_columns('submit_status');
			
	// 		$output = $crud->render();

	// 		// $this->_example_output($output);
	// 		$this->load->view('fa/approve_list.php',(array)$output);

	// }

	// callback function area

	// for update request date to now when new request number
	public function update_request_date($post_array,$primary_key){

		$req_number = $primary_key;

		// $username = $this->session->userdata('name');
		$username = $this->session->userdata('user_id');
		date_default_timezone_set("Asia/Bangkok");
		$request_date = date("Y-m-d H:i:s");
		// // update request date to now
		$this->Date_model->update_request_date($req_number,$username,$request_date);

		$submit_status = $post_array['submit_status'];

		if($submit_status == '2'){
			$this->Request_model->insert_approve_level($req_number);
		}
		

		return $post_array;
	}

	// for insert table t_approve_level when submit request to approver
	public function submit_request($post_array,$primary_key){

		$req_number = $primary_key;

		$submit_status = $post_array['submit_status'];

		if($submit_status == '2'){
			$this->Request_model->insert_approve_level($req_number);
		}
		

		return $post_array;
	}
}

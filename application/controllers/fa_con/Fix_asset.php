<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fix_asset extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
		// $this->load->model('itasset/Vendor_model', 'Vendor_model');
	}

	public function _example_output($output = null)
	{
		$this->load->view('fa/fix_asset.php',(array)$output);
		
		// $this->load->view('example',(array)$output);
	}

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function item_asset_management_log(){

		$id = $this->uri->segment(4);

		$crud = new grocery_CRUD();
		$crud->set_theme('flexigrid');
		$crud->set_table('v_item_asset_log');
		$crud->set_primary_key('last_updated','v_item_asset_log');
		$crud->order_by('last_updated','desc');
		$crud->where('asset_id' ,$id );

		// $crud->set_relation('asset_type','t_asset_type','asset_type_name',array('asset_type_status' => '1'),'asset_type_name asc');
		$crud->display_as('Type','Type');

		// $crud->set_relation('location','t_location','location',array('loดcation_status' => '1'),'location asc');
		$crud->display_as('Location','Location');

		// $crud->set_relation('vendor_contact','t_vendor','vendor_name',array('vendor_status' => '1'),'vendor_name asc');
		$crud->display_as('vendor_name','Vendor Contact');

		// $crud->set_relation('asset_owner','t_asset_owner','{employee_id} - {first_name} {last_name}',array('asset_owner_status' => '1'),'first_name asc');
		$crud->display_as('asset_owner','Asset Owner');

		// $crud->set_relation('use_status','t_use_status','use_description',null,'use_status desc');
		
		$crud->display_as('asset_owner','Asset Owner');
		$crud->display_as('use_status','Usage Status');

		$crud->columns('last_updated','user_updated','asset_name','serial_number'
		,'usage_status','location' , 'employee_id','department_name'
		,'asset_code_number','asset_type_name'
		,'installation_date','purchase_date','expire_date'
		// 'vendor_contact',
		
		);	

		$crud->set_subject('Item Asset History Log');
		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();

		$output = $crud->render();

		$this->_example_output($output);

	}

	public function fix_asset_management()
	{
			$sc_id = "";

			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_fa');
			// $crud->where('right(costcenter,3) =', '536');	
			
			if(strlen($this->uri->segment(4)) == '3'){
				$sc_id = $this->uri->segment(4);
				$crud->where('right(costcenter,3)=', '536');
			}

			$crud->set_relation('dep_key','t_dep_key','{dep_key_id} ( {year} year / {percentage} % )',array('status' => '1'),'dep_key_id asc');
			$crud->display_as('dep_key_id','Depre Key');

			$crud->set_relation('asset_class','t_asset_class','{asset_class_id} ( {asset_bs_account} )',array('status' => '1'),'asset_class_id asc');
			$crud->display_as('asset_class_id','Asset Class');

			$crud->set_relation('costcenter','t_costcenter','costcenter_id',array('status' => '1'),'costcenter_id asc');
			$crud->display_as('costcenter_id','Cost Center');
			
			// $crud->set_relation('asset_type','t_asset_type','asset_type_name',array('asset_type_status' => '1'),'asset_type_name asc');
            // $crud->display_as('Type','Type');

            // $crud->set_relation('location','t_location','location',array('location_status' => '1'),'location asc');
            // $crud->display_as('Location','Location');

            // $crud->set_relation('vendor_contact','t_vendor','vendor_name',array('vendor_status' => '1'),'vendor_name asc');
            // $crud->display_as('vendor_name','Vendor Contact');

            // $crud->set_relation('asset_owner','t_asset_owner','{employee_id} - {first_name} {last_name}',array('asset_owner_status' => '1'),'first_name asc');
            // $crud->display_as('asset_owner','Asset Owner');

            // $crud->set_relation('use_status','t_use_status','use_description',null,'use_status desc');
            // $crud->display_as('asset_owner','Asset Owner');
            // $crud->display_as('use_status','Usage Status');

            // $crud->columns('qr_code_img','asset_image','asset_name','serial_number','asset_code_number','asset_type','location',
			// // 'installation_date','purchase_date','expire_date',
			// 'vendor_contact','asset_owner','use_status'
            // );	
			// $crud->set_relation('category_status','t_use_status','status',null,'status desc');

			$crud->field_type('status', 'hidden', 3);
			$crud->field_type('created_date', 'hidden', 3);
			
			$crud->add_action('Print QRCode', ''.base_url().'assets/images/qrcode.png', 'fa_con/fix_asset/print_qrcode',$action_target = 'qrcode_icon');
			// $crud->add_action('History log', ''.base_url().'assets/images/log.png', 'fa_con/fix_asset/item_asset_management_log',$action_target = 'log_icon');
			
			$crud->callback_before_insert(array($this,'set_qr_code_id'));
			$crud->callback_before_update(array($this,'set_qr_code_id'));
			// $crud->display_as('status','status');
			
			// $crud->callback_before_update(array($this,'insert_log'));
			// $crud->callback_after_update(array($this,'updated_record'));
            
			$crud->set_subject('Fix Asset');

			// $crud->required_fields('category_name');
			// $crud->required_fields('category_status');

			$crud->set_field_upload('qr_code_img','assets/images/qrcode');

            $crud->set_field_upload('asset_image','assets/uploads/asset_images');
            // $crud->set_field_upload('reference_document','assets/uploads/files');
            

            $crud->field_type('currency','readonly');
            // $crud->field_type('last_updated','hidden');
			// $crud->field_type('user_updated','hidden');

			$crud->add_action('History log', ''.base_url().'assets/images/log.png', 'fa_con/fix_asset/view_history_log',$action_target = 'log_icon');
			
			$crud->unset_read();

			$crud->unset_delete();

			$output = $crud->render();
			// $crud->unset_jquery();
			$this->_example_output($output);
			
	}

	public function view_history_log($post_array){

		$asset_id = $this->uri->segment(4);

		$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('v_history_log');
			$crud->set_primary_key('trans_id','v_history_log');

			$crud->where('asset_id =', $asset_id);			

			$crud->set_subject('History Log');

			$output = $crud->render();

			$this->_example_output($output);
	

	}

	public function insert_log($post_array, $primary_key){
			$asset_id = $primary_key;
			$username = $this->session->userdata('name');
			$this->load->model('itasset/Item_asset_model', 'Item_asset_model');
			$this->Item_asset_model->insert_item_asset_log($asset_id,$username);

			

			return $post_array;
	}	

	public function updated_record ($post_array, $primary_key){
		
			$asset_id = $primary_key;
			$username = $this->session->userdata('name');
			date_default_timezone_set("Asia/Bangkok");
			$last_updated = date("Y-m-d H:i:s");
			// update last update username and date to item_asset
			$this->Item_asset_model->updated_item_asset_record($asset_id,$username,$last_updated);

			return $post_array;
	}

	// use function print qr_code
	public function print_qrcode_img(){

		$id = $this->uri->segment(4);
		$this->load->model('fix-asset/Fix_asset_model', 'Fix_asset_model');
		$data['item_detail'] =  $this->Fix_asset_model->get_item_by_id($id);

		$this->load->view("itasset/layout-qr_code.php", $data);

	}

	public function print_qrcode(){

		// $username = $this->session->userdata('name');

		$id = $this->uri->segment(4);
		$this->load->model('fa/Fix_asset_model', 'Fix_asset_model');
		// $data['item_detail'] =  $this->Item_asset_model->get_item_by_id($id);
		$item = $this->Fix_asset_model->get_item_by_id($id);

		$asset_name = $item[0]['asset_no'];
		$serial_no = $item[0]['invent_no'];
		$qrcode_img = $item[0]['qr_code_img'];
		

		// $this->load->view("itasset/layout-qr_code.php", $data);

		// require(APPPATH .'fpdf/fpdf.php'); 
		require(APPPATH .'fpdf/Mypdf.php'); 
		// echo APPPATH . 'fpdf/fpdf.php' ;

		//   $pdf = new FPDF('p','mm', array(48,24));
		  $pdf = new Mypdf('p','mm', array(28,24));
		  $pdf->SetAutoPageBreak(false);
		  $pdf-> AddPage('L');
		  
		// Insert a dynamic image from a URL
		  $pdf->Image(base_url().'/assets/images/qrcode/'.$qrcode_img,2,3,18,0,'PNG');

		//   $pdf->SetXY(2, -5);
		//   $pdf->RotatedText(5,250,"Example of rotated text",90);

		  $pdf->SetFont('Arial','',5);
	      $pdf->SetXY(21,21);
		  $pdf->Rotate(90); 
		  $pdf->Cell(0,0,"AST:$asset_name",0,0,'L'); 
		//   $pdf->Cell(0,0,"$username",0,0,'L'); 
		//   $pdf->RotatedText(5,250,"Example of rotated text",90);

		// $pdf->SetFont('Arial','',4.5);
		// $pdf->SetXY(21,23);
		// // $pdf->SetXY(22, 11 );
		// // $pdf->Rotate(0); 
		// $pdf->Cell(0,0,"INV:$serial_no",0,0,'L'); 
		  
		  

		//   $pdf->SetFont('Arial','',7);
		//   $pdf->SetXY(33, 16 );
		//   $pdf->Cell(10,0,"$serial_no",0,0,'R'); 


		  ob_start(); 
		  $pdf -> output ($_SERVER["DOCUMENT_ROOT"].'/assets/images/qrcode_pdf'. 'your_file_pdf.pdf','I');
		  ob_end_flush(); 

	}

	public function set_qr_code_id($post_array){

		
		
		if(empty($post_array['qr_code_id']))
		{
				$qr_code = strtoupper(uniqid());
				// $barcode_id = '*'.$barcode.'*';

				$this->load->library('ciqrcode');

				// $config['cacheable']	= true; //boolean, the default is true
				// $config['cachedir']		= ''; //string, the default is application/cache/
				// $config['errorlog']		= ''; //string, the default is application/logs/
				// $config['quality']		= true; //boolean, the default is true
				// $config['size']			= ''; //interger, the default is 1024
				// $config['black']		= array(224,255,255); // array, default is array(255,255,255)
				// $config['white']		= array(70,130,180); // array, default is array(0,0,0)
				// $this->ciqrcode->initialize($config);

				$qr_gen = base_url()."api/qr_scan/scan?qr_code_id=".$qr_code;

				// $params['data'] = $qr_code;
				$params['data'] = $qr_gen;
				$params['level'] = 'H';
				$params['size'] = 10;
				$params['savename'] = "assets/images/qrcode/$qr_code.png";

				$qrcode_image = "$qr_code.png" ;
				$this->ciqrcode->generate($params);



				$post_array['qr_code_id'] = $qr_code ;	
				$post_array['qr_code_img'] = $qrcode_image ;	

		}
		else
		{
		// unset($post_array['item_barcode']);
			unset($post_array['qr_code_id']);
		}

		// $barcode_id = strtoupper(uniqid());
		// $post_array['item_barcode'] = $barcode_id ;
		
		return $post_array;
	}


	public function unassign_owner()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_item_asset');
			$crud->where('asset_owner = ',' ');
			$crud->set_relation('asset_type','t_asset_type','asset_type_name',array('asset_type_status' => '1'),'asset_type_name asc');
            $crud->display_as('Type','Type');

            $crud->set_relation('location','t_location','location',array('location_status' => '1'),'location asc');
            $crud->display_as('Location','Location');

            $crud->set_relation('vendor_contact','t_vendor','vendor_name',array('vendor_status' => '1'),'vendor_name asc');
            $crud->display_as('vendor_name','Vendor Contact');

            $crud->set_relation('asset_owner','t_asset_owner','{employee_id} - {first_name} {last_name}',array('asset_owner_status' => '1'),'first_name asc');
            $crud->display_as('asset_owner','Asset Owner');

            $crud->set_relation('use_status','t_use_status','use_description',null,'use_status desc');
            $crud->display_as('asset_owner','Asset Owner');
            $crud->display_as('use_status','Usage Status');

            $crud->columns('qr_code_img','asset_image','asset_name','serial_number','asset_code_number','asset_type','location',
			// 'installation_date','purchase_date','expire_date',
			'vendor_contact','asset_owner','use_status'
            );	
            // $crud->set_relation('category_status','t_use_status','status',null,'status desc');
            // $crud->display_as('status','status');
			
			$crud->add_action('Print QR Code', ''.base_url().'assets/images/qrcode.png', 'itasset/item_asset/print_qrcode',$action_target = 'qrcode_icon');
			$crud->add_action('History log', ''.base_url().'assets/images/log.png', 'itasset/item_asset/item_asset_management_log',$action_target = 'log_icon');
			
			$crud->set_subject('Item Asset');

			// $crud->required_fields('category_name');
			// $crud->required_fields('category_status');

			$crud->set_field_upload('qr_code_img','assets/images/qrcode');
            $crud->set_field_upload('asset_image','assets/uploads/asset_images');
            $crud->set_field_upload('reference_document','assets/uploads/files');
            

            $crud->field_type('qr_code_id','hidden');
            $crud->field_type('last_updated','hidden');
			$crud->field_type('user_updated','hidden');
			
			$crud->unset_add();
			$crud->unset_delete();

			$output = $crud->render();

			//$this->_example_output($output);
			$this->load->view('itasset/layout-unassign_owner.php',(array)$output);
	}

	public function assigned_owner()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_item_asset');
			$crud->where('asset_owner <> ',' ');
			$crud->set_relation('asset_type','t_asset_type','asset_type_name',array('asset_type_status' => '1'),'asset_type_name asc');
            $crud->display_as('Type','Type');

            $crud->set_relation('location','t_location','location',array('location_status' => '1'),'location asc');
            $crud->display_as('Location','Location');

            $crud->set_relation('vendor_contact','t_vendor','vendor_name',array('vendor_status' => '1'),'vendor_name asc');
            $crud->display_as('vendor_name','Vendor Contact');

            $crud->set_relation('asset_owner','t_asset_owner','{employee_id} - {first_name} {last_name}',array('asset_owner_status' => '1'),'first_name asc');
            $crud->display_as('asset_owner','Asset Owner');

            $crud->set_relation('use_status','t_use_status','use_description',null,'use_status desc');
            $crud->display_as('asset_owner','Asset Owner');
            $crud->display_as('use_status','Usage Status');
			
			$crud->columns('qr_code_img','asset_image','asset_name','serial_number','asset_code_number','asset_type','location',
			// 'installation_date','purchase_date','expire_date',
			'vendor_contact','asset_owner','use_status'
            );	
            // $crud->set_relation('category_status','t_use_status','status',null,'status desc');
            // $crud->display_as('status','status');
			
			$crud->add_action('Print QR Code', ''.base_url().'assets/images/qrcode.png', 'itasset/item_asset/print_qrcode',$action_target = 'qrcode_icon');
			$crud->add_action('History log', ''.base_url().'assets/images/log.png', 'itasset/item_asset/item_asset_management_log',$action_target = 'log_icon');

			$crud->set_subject('Item Asset');

			// $crud->required_fields('category_name');
			// $crud->required_fields('category_status');

			$crud->set_field_upload('qr_code_img','assets/images/qrcode');
            $crud->set_field_upload('asset_image','assets/uploads/asset_images');
            $crud->set_field_upload('reference_document','assets/uploads/files');
            

            $crud->field_type('qr_code_id','hidden');
            $crud->field_type('last_updated','hidden');
			$crud->field_type('user_updated','hidden');
			
			$crud->unset_add();
			$crud->unset_delete();

			$output = $crud->render();

			//$this->_example_output($output);
			$this->load->view('itasset/layout-assign_owner.php',(array)$output);
	}

	public function asset_expired()
	{
			$crud = new grocery_CRUD();

			$date_now = date("Y-m-d H:i:s");
			
			$crud->set_theme('flexigrid');
			$crud->set_table('t_item_asset');
			$crud->where('expire_date < ',$date_now);
			$crud->set_relation('asset_type','t_asset_type','asset_type_name',array('asset_type_status' => '1'),'asset_type_name asc');
            $crud->display_as('Type','Type');

            $crud->set_relation('location','t_location','location',array('location_status' => '1'),'location asc');
            $crud->display_as('Location','Location');

            $crud->set_relation('vendor_contact','t_vendor','vendor_name',array('vendor_status' => '1'),'vendor_name asc');
            $crud->display_as('vendor_name','Vendor Contact');

            $crud->set_relation('asset_owner','t_asset_owner','{employee_id} - {first_name} {last_name}',array('asset_owner_status' => '1'),'first_name asc');
            $crud->display_as('asset_owner','Asset Owner');

            $crud->set_relation('use_status','t_use_status','use_description',null,'use_status desc');
            $crud->display_as('asset_owner','Asset Owner');
            $crud->display_as('use_status','Usage Status');

            $crud->columns('asset_image','asset_name','asset_description','serial_number','asset_code_number','asset_type','location',
            'installation_date','purchase_date','expire_date','vendor_contact','asset_owner','use_status'
            );	
            // $crud->set_relation('category_status','t_use_status','status',null,'status desc');
            // $crud->display_as('status','status');
			$crud->add_action('Print QR Code', ''.base_url().'assets/images/qrcode.png', 'itasset/item_asset/print_qrcode',$action_target = 'qrcode_icon');
			
			$crud->set_subject('Item Asset');

			// $crud->required_fields('category_name');
			// $crud->required_fields('category_status');

            $crud->set_field_upload('asset_image','assets/uploads/asset_images');
            $crud->set_field_upload('reference_document','assets/uploads/files');
            

            $crud->field_type('qr_code_id','hidden');
            $crud->field_type('last_updated','hidden');
			$crud->field_type('user_updated','hidden');
			
			$crud->unset_add();
			$crud->unset_delete();

			$output = $crud->render();

			//$this->_example_output($output);
			$this->load->view('itasset/layout-assign_owner.php',(array)$output);
	}

	public function asset_expired_in_days()
	{

			$expire_days = $this->uri->segment(4);
			$view = "v_item_asset_expire_1";

			switch ($expire_days) {
				case 1:
					$view = 'v_item_asset_expire_1';
					break;
				case 2:
					$view = 'v_item_asset_expire_2';
					break;
				case 3:
					$view = 'v_item_asset_expire_3';
					break;
			}
			

			$crud = new grocery_CRUD();

			$date_now = date("Y-m-d H:i:s");
			
			$crud->set_theme('flexigrid');
			$crud->set_table($view);
			// $crud->where('datediff(expire_date,now()) between < '0 and 30);
			$crud->set_primary_key('asset_id',$view);
			$crud->set_relation('asset_type','t_asset_type','asset_type_name',array('asset_type_status' => '1'),'asset_type_name asc');
            $crud->display_as('Type','Type');

            $crud->set_relation('location','t_location','location',array('location_status' => '1'),'location asc');
            $crud->display_as('Location','Location');

            $crud->set_relation('vendor_contact','t_vendor','vendor_name',array('vendor_status' => '1'),'vendor_name asc');
            $crud->display_as('vendor_name','Vendor Contact');

            $crud->set_relation('asset_owner','t_asset_owner','{employee_id} - {first_name} {last_name}',array('asset_owner_status' => '1'),'first_name asc');
            $crud->display_as('asset_owner','Asset Owner');

            $crud->set_relation('use_status','t_use_status','use_description',null,'use_status desc');
            $crud->display_as('asset_owner','Asset Owner');
            $crud->display_as('use_status','Usage Status');

            $crud->columns('days_to_expire','expire_date','asset_name','asset_description','serial_number','asset_code_number','asset_type','location',
            'installation_date','purchase_date','vendor_contact','asset_owner','use_status'
            );	
            // $crud->set_relation('category_status','t_use_status','status',null,'status desc');
            // $crud->display_as('status','status');
			$crud->add_action('Print QR Code', ''.base_url().'assets/images/qrcode.png', 'itasset/item_asset/print_qrcode',$action_target = 'qrcode_icon');
			
			$crud->set_subject('Item Asset');

			// $crud->required_fields('category_name');
			// $crud->required_fields('category_status');

            $crud->set_field_upload('asset_image','assets/uploads/asset_images');
            $crud->set_field_upload('reference_document','assets/uploads/files');
            

            $crud->field_type('qr_code_id','hidden');
            $crud->field_type('last_updated','hidden');
			$crud->field_type('user_updated','hidden');
			
			$crud->unset_add();
			$crud->unset_delete();

			$output = $crud->render();

			//$this->_example_output($output);
			$this->load->view('itasset/layout-expire_in_days.php',(array)$output);
	}

	public function usage_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_use_status');
			$crud->display_as('use_description','Usage Status');
			$crud->set_subject('Usage Status');

			$crud->required_fields('use_status');
			$crud->required_fields('use_description');

			// $crud->set_field_upload('center_map','assets/uploads/files');
			// // $crud->field_type('center_code','readonly');

			$output = $crud->render();

			$this->_example_output($output);
	}

	public function get_netbook_value($asset_id)
	{
	
			//http://localhost/inventory/admin/unit/get_item_unit/2
			//change talble to view
			$query = $this->db->query("SELECT CAST(book_val AS DECIMAL(10,2)) book_val FROM t_fa where asset_id = '$asset_id'");
				
			

			if( $query->num_rows() > 0) {
				$result = $query->result(); //or $query->result_array() to get an array
				// print_r($result);

				// echo "<select id='field_unit_chosen' name = 'unit'>";
				foreach( $result as $row )
				{
					echo ($row->book_val);
				// 	// echo 	"<option value='".$row->item_um_code."'>".$row->unit.' / '.$row->base_quantity." unit</option>";
					// echo "<input id='field-net_value' class='form-control' name='net_value' value='$row->book_val' maxlength='11' type='text'>";
				}
				// // echo "</select>";
			}
			
	}



}

<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Request_new extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
		$this->load->model('fa/Date_model', 'Date_model');
		$this->load->model('fa/Request_model', 'Request_model');
		


	}

	public function _example_output($output = null)
	{
		$this->load->view('fa/request_detail_edit.php',(array)$output);
	}

	public function _example_output_request_detail($output = null)
	{
		$this->load->view('fa/request_detail.php',(array)$output);
	}

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function new_request()
	{
		$this->config->set_item('grocery_crud_dialog_forms',true);
			$user_id = $this->session->userdata('user_id');

			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_request_header');			

			$crud->set_relation('req_type','t_request_type','type_name',null,'type_id asc');
			$crud->display_as('req_type','Request for');
			
			$crud->set_relation('user_request','ci_users','username',null);
			$crud->display_as('user_request','User Request');

			$crud->set_relation('dept_id','t_department','{department_id} - {function_description} - {function_name}',null,'department_id asc');
			$crud->display_as('dept_id','function_name');

			$crud->set_relation('it_asset','t_it_asset','it_asset_description',null);
			$crud->display_as('it_asset','IT Asset');

            // ****** Show Draft Status only
            $crud->where('submit_status =','1');
            $crud->where('user_request =',$user_id);
		    
			
            // ****** list asset in own location only
            // change to query v_asset_for_request
           
            
             // ****** list locatin is not Costcenter asset current // $crud->set_relation('asset_id','t_fa','{asset_no}-{asset_description}-{invent_no}',null,'asset_id asc');
            // $crud->display_as('asset_id','Fix Asset');
             // change to query v_location
            //  $crud->set_relation('to_costcenter','t_costcenter','{costcenter_id}',null,'costcenter_id asc');
			//  $crud->display_as('costcenter_id','To Costcenter');
			$crud->set_primary_key('costcenter_id','v_costcenter_mapping');
			$crud->set_relation('to_costcenter','v_costcenter_mapping','{costcenter_id} {function_description}-{function_name}-{sc_name}',null,'costcenter_id asc');
			$crud->display_as('costcenter_id','To Costcenter');
			

            $crud->set_relation('submit_status','t_submit_status','submit_description',null,'submit_id asc');
            $crud->display_as('submit_id','Submit Status');

			$crud->set_subject('New Request');

			$crud->required_fields('req_type','dept_id','it_asset');

            // $crud->required_fields('submit_status');
            // $crud->required_fields('percentage');
            // $crud->required_fields('status');

			$crud->field_type('req_number', 'readonly');
			$crud->field_type('request_date', 'hidden');
			$crud->field_type('user_request', 'hidden');
		

			$crud->unset_delete();
			$crud->unset_read();

			$crud->unset_fields('user_request');

			$crud->display_as('req_number','Request No.');

			
			// **** for update request_date to now
				$crud->callback_after_insert(array($this, 'update_request_date'));

			// **** for insert t_approve_level when user submit request
				// $crud->callback_after_insert(array($this, 'submit_request'));
				$crud->callback_after_update(array($this, 'submit_request'));

			$crud->columns('request_date','req_number','req_type','dept_id','to_costcenter','it_asset','submit_status','user_request');
			$crud->order_by('request_date','desc');
			// $crud->unset_jquery();

			$crud->add_action('Request Detail', ''.base_url().'assets/images/add2.png', 'fa_con/Request_new/Request_detail',$action_target = 'request_detail');
			// $crud->add_action('Request Detail', ''.base_url().'assets/images/add2.png', 'examples/multigrids',$action_target = 'request_detail');

			
			if($crud->getState() == 'add') {
				// $crud->unset_fields('submit_status');
				$crud->unset_fields('user_request','submit_status');
				// echo "add";
			} 

			$output = $crud->render();

				
		
			// $this->_example_output($output);
			$this->load->view('fa/owner_request.php',(array)$output);
	}
	

	public function wait_for_approve(){
			
		$user_id = $this->session->userdata('user_id');
		
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_request');			

			$crud->set_relation('req_type','t_request_type','type_name',null,'type_id asc');
			$crud->display_as('req_type','Request for');

			$crud->set_relation('user_request','ci_users','username',null);
			$crud->display_as('user_request','User Request');

			$crud->set_primary_key('req_number','v_request_status');
			$crud->set_relation('req_number','v_request_status','req_number',null);
			$crud->display_as('req_status','Request Status');
	
			// ****** Show Draft Status only
			$crud->where('submit_status =','2');	
			$crud->where('req_status =','1');	
			$crud->where('user_request =',$user_id);

			// ****** list asset in own location only
            // change to query v_asset_for_request
            $crud->set_relation('asset_id','t_fa','{asset_no}-{asset_description}-{invent_no}',null,'asset_id asc');
            $crud->display_as('asset_id','Fix Asset');
            
             // ****** list locatin is not Costcenter asset current
             // change to query v_location
            //  $crud->set_relation('to_costcenter','t_costcenter','{costcenter_id}',null,'costcenter_id asc');
			//  $crud->display_as('costcenter_id','To Costcenter');
			$crud->set_primary_key('costcenter_id','v_costcenter_mapping');
			$crud->set_relation('to_costcenter','v_costcenter_mapping','{costcenter_id} {function_description}-{function_name}-{sc_name}',null,'costcenter_id asc');
			$crud->display_as('costcenter_id','To Costcenter');
			

            $crud->set_relation('submit_status','t_submit_status','submit_description',null,'submit_id asc');
			$crud->display_as('submit_id','Submit Status');

			$crud->display_as('req_number','Request No.');
			

			$crud->set_subject('Wait for Approve');

			$crud->columns('request_date','req_number','req_type','asset_id','to_costcenter','submit_status','user_request');
			$crud->order_by('request_date','desc');


			$crud->add_action('Approve List', ''.base_url().'assets/images/log.png', 'fa_con/request/approve_list/t',$action_target = 'log_icon');


			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			
			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('fa/owner_submit.php',(array)$output);
	}

	public function request_approved(){
			
		
		$user_id = $this->session->userdata('user_id');

		$crud = new grocery_CRUD();

		$crud->set_theme('flexigrid');
		$crud->set_table('t_request');			

		$crud->set_relation('req_type','t_request_type','type_name',null,'type_id asc');
		$crud->display_as('req_type','Request for');

		$crud->set_primary_key('req_number','v_request_status');
		$crud->set_relation('req_number','v_request_status','req_number',null);
		$crud->display_as('req_status','Request Status');

		$crud->set_relation('user_request','ci_users','username',null);
		$crud->display_as('user_request','User Request');

		// ****** Show Draft Status only
		$crud->where('submit_status =','2');	
		$crud->where('req_status =','2');	
		$crud->where('user_request =',$user_id);

		// ****** list asset in own location only
		// change to query v_asset_for_request
		$crud->set_relation('asset_id','t_fa','{asset_no}-{asset_description}-{invent_no}',null,'asset_id asc');
		$crud->display_as('asset_id','Fix Asset');
		
		 // ****** list locatin is not Costcenter asset current
		 // change to query v_location
		//  $crud->set_relation('to_costcenter','t_costcenter','{costcenter_id}',null,'costcenter_id asc');
		//  $crud->display_as('costcenter_id','To Costcenter');
		$crud->set_primary_key('costcenter_id','v_costcenter_mapping');
		$crud->set_relation('to_costcenter','v_costcenter_mapping','{costcenter_id} {function_description}-{function_name}-{sc_name}',null,'costcenter_id asc');
		$crud->display_as('costcenter_id','To Costcenter');
		

		$crud->set_relation('submit_status','t_submit_status','submit_description',null,'submit_id asc');
		$crud->display_as('submit_id','Submit Status');

		$crud->display_as('req_number','Request No.');
		

		$crud->set_subject('Request Approved');

		$crud->columns('request_date','req_number','req_type','asset_id','to_costcenter','submit_status','user_request');
		$crud->order_by('request_date','desc');


		$crud->add_action('Approve List', ''.base_url().'assets/images/log.png', 'fa_con/request/approve_list/t',$action_target = '_self');


		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();
		
		$output = $crud->render();

		// $this->_example_output($output);
		$this->load->view('fa/request_approved.php',(array)$output);
	}

	public function request_reject(){
			
		$user_id = $this->session->userdata('user_id');

		$crud = new grocery_CRUD();

		$crud->set_theme('flexigrid');
		$crud->set_table('t_request');			

		$crud->set_relation('req_type','t_request_type','type_name',null,'type_id asc');
		$crud->display_as('req_type','Request for');

		$crud->set_primary_key('req_number','v_request_status');
		$crud->set_relation('req_number','v_request_status','req_number',null);
		$crud->display_as('req_status','Request Status');

		// $crud->set_relation('v_request_status.approve_status','t_approve_status','approve_status_description',null);
		// $crud->display_as('approve_status_description','Approve Status');

		$crud->set_relation('user_request','ci_users','username',null);
		$crud->display_as('user_request','User Request');

		// ****** Show Draft Status only
		$crud->where('submit_status =','2');	
		$crud->where('req_status =','3');	
		$crud->where('user_request =',$user_id);

		// ****** list asset in own location only
		// change to query v_asset_for_request
		$crud->set_relation('asset_id','t_fa','{asset_no}-{asset_description}-{invent_no}',null,'asset_id asc');
		$crud->display_as('asset_id','Fix Asset');
		
		 // ****** list locatin is not Costcenter asset current
		 // change to query v_location
		//  $crud->set_relation('to_costcenter','t_costcenter','{costcenter_id}',null,'costcenter_id asc');
		//  $crud->display_as('costcenter_id','To Costcenter');
		$crud->set_primary_key('costcenter_id','v_costcenter_mapping');
		$crud->set_relation('to_costcenter','v_costcenter_mapping','{costcenter_id} {function_description}-{function_name}-{sc_name}',null,'costcenter_id asc');
		$crud->display_as('costcenter_id','To Costcenter');
		

		$crud->set_relation('submit_status','t_submit_status','submit_description',null,'submit_id asc');
		$crud->display_as('submit_id','Submit Status');

		$crud->display_as('req_number','Request No.');
		

		$crud->set_subject('Request Reject');

		$crud->columns('request_date','req_number','req_type','asset_id','to_costcenter','submit_status','user_request');
		$crud->order_by('request_date','desc');


		$crud->add_action('Approve List', ''.base_url().'assets/images/log.png', 'fa_con/request/approve_list/t',$action_target = 'log_icon');


		$crud->unset_add();
		$crud->unset_edit();
		$crud->unset_delete();
		
		$output = $crud->render();

		// $this->_example_output($output);
		$this->load->view('fa/request_reject.php',(array)$output);
	}

	public function approve_list(){
			
			$req_check_query = $this->uri->segment(4);
			$req_number = $this->uri->segment(5);

			

			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('v_role_approve_user');	
			$crud->set_primary_key('trans_id','v_role_approve_user');
			// $crud->where('req_number =', $req_number);	
			switch ($req_check_query) {
				case "t":
				$crud->where('req_number =', $req_number);	
					break;
				case "v":

				
				$item =  $this->Request_model->get_req_number_from_trans_id($req_number);
				$req_number = $item[0]['req_number'];
				print_r($item);

				$crud->where('req_number =', $req_number);	
					break;
			}

			$crud->set_relation('role_id','ci_user_groups','group_name',null);
			$crud->display_as('group_name','Role');

			$crud->display_as('role_id','Role');

			$crud->set_relation('approve_status','t_approve_status','approve_status_description',null);
			$crud->display_as('approve_status_description','Approve Status');

			$crud->set_relation('user_approver','ci_users','{firstname} {lastname} [{username}]',null);
			$crud->display_as('username','User Approver');

			$crud->display_as('req_number','Request No.');
			$crud->display_as('it_approve','Need IT Approve');

			$crud->columns('req_number','review','approve_status','role_id','user_approver','approve_min','approve_max','book_val','it_approve','asset_no','asset_description','costcenter','to_costcenter');
			$crud->order_by('role_id','asc');

			$crud->unset_operations();
			$crud->unset_columns('submit_status');
			
			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('fa/approve_list.php',(array)$output);

	}

	// for new menu
	public function transfer_request_status(){
		$request_status = $this->uri->segment(4);

		$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('v_request_header_show');	
			$crud->set_primary_key('req_number','v_request_header_show');
			$crud->where('req_type =','1');	

			switch ($request_status) {
				case "1": // draft
				$crud->where('submit_status =', '1');	
					break;
				case "2": // submit - wait for approve
				$crud->where('submit_status =', '2');
				$crud->where('request_status =', '1');
				$crud->unset_delete();
					break;
				case "3": // submit - approved
				$crud->where('submit_status =', '2');
				$crud->where('request_status =', '2');
				$crud->unset_delete();
					break;
				case "4": // submit - reject
				$crud->where('submit_status =', '2');
				$crud->where('request_status =', '3');
				$crud->unset_delete();
					break;
			}

			$crud->add_action('Request Detail', ''.base_url().'assets/images/view_detail.png', 'fa_con/Request_new/request_detail_view',$action_target = 'request_detail');
			$crud->add_action('Approve List', ''.base_url().'assets/images/people.png', 'fa_con/Request_new/approve_list_view',$action_target = 'approve_list_view');
			// $crud->set_relation('role_id','ci_user_groups','group_name',null);
			// $crud->display_as('group_name','Role');
			$crud->unset_columns('req_type','submit_status','request_status','user_request','role_id');
			// $crud->unset_operations();
			$crud->unset_add();
			$crud->unset_edit();
		
			$crud->unset_read();
			$output = $crud->render();
			$this->load->view('fa/transfer_request_view.php',(array)$output);
	}

	public function disposal_request_status(){
		$request_status = $this->uri->segment(4);

		$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('v_request_header_show');	
			$crud->set_primary_key('req_number','v_request_header_show');
			$crud->where('req_type =','2');	

			switch ($request_status) {
				case "1": // draft
				$crud->where('submit_status =', '1');	
					break;
				case "2": // submit - wait for approve
				$crud->where('submit_status =', '2');
				$crud->where('request_status =', '1');
					break;
				case "3": // submit - approved
				$crud->where('submit_status =', '2');
				$crud->where('request_status =', '2');
					break;
				case "4": // submit - reject
				$crud->where('submit_status =', '2');
				$crud->where('request_status =', '3');
					break;
			}

			$crud->add_action('Request Detail', ''.base_url().'assets/images/view_detail.png', 'fa_con/Request_new/request_detail_view',$action_target = 'request_detail');
			$crud->add_action('Approve List', ''.base_url().'assets/images/people.png', 'fa_con/Request_new/approve_list_view',$action_target = 'approve_list_view');
			// $crud->set_relation('role_id','ci_user_groups','group_name',null);
			// $crud->display_as('group_name','Role');
			$crud->unset_columns('req_type','submit_status','request_status','user_request','role_id');
			// $crud->unset_operations();
			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			$crud->unset_read();
			$output = $crud->render();
			$this->load->view('fa/disposal_request_view.php',(array)$output);
	}

	public function request_detail_view(){
		$req_number = $this->uri->segment(4);

		$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('v_request_detail');	
			$crud->set_primary_key('req_num','v_request_detail');
			$crud->where('req_num =',$req_number);	

			$crud->unset_columns('request_detail_id','asset_id','asset_image','book_val','role_id');

			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			$crud->unset_read();
			$output = $crud->render();
			$this->load->view('fa/transfer_request_view.php',(array)$output);

	}

	public function approve_list_view(){

		$req_number = $this->uri->segment(4);

		$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('v_request_approve_list');	
			$crud->set_primary_key('req_number','v_request_approve_list');
			$crud->where('req_number =',$req_number);	

			$crud->unset_columns('req_type','user_approve_id','user_request_id','role_requestor','role_approve_id','level_approve','approve_status','approve_date');

			$crud->unset_add();
			$crud->unset_edit();
			$crud->unset_delete();
			$crud->unset_read();
			$output = $crud->render();
			$this->load->view('fa/approve_list_view.php',(array)$output);
	}

	// callback function area

	// for update request date to now when new request number
	public function update_request_date($post_array,$primary_key){

		$req_number = $primary_key;

		// $username = $this->session->userdata('name');
		$username = $this->session->userdata('user_id');
		date_default_timezone_set("Asia/Bangkok");
		$request_date = date("Y-m-d H:i:s");
		// // update request date to now
		$this->Date_model->update_request_header_date($req_number,$username,$request_date);

		$submit_status = $post_array['submit_status'];

		if($submit_status == '2'){
			$this->Request_model->insert_approve_level($req_number);
		}
		

		return $post_array;
	}

	// for insert table t_approve_level when submit request to approver
	public function submit_request($post_array,$primary_key){

		$req_number = $primary_key;

		$submit_status = $post_array['submit_status'];

		if($submit_status == '2'){
		
			$result = $this->Request_model->insert_approve_level($req_number);

			if($result){
				
				$this->Request_model->update_user_approver_level($req_number);
			}

		}

		return $post_array;
	}

	public function request_detail(){


		$req_number = $this->uri->segment(4);

		if($this->uri->segment(4)){
			// echo "Value from uri3  ";
			// echo ($this->uri->segment(3));
			$req_no = $this->uri->segment(4);
			$this->session->set_userdata('req_num', $req_number);
			// echo "set value uri3 to session ";
		}else{
			// echo "Value from session ";
			// echo($this->session->userdata('req_num'));
			$req_no = $this->session->userdata('req_num');
		}


				$item =  $this->Request_model->get_request_header_description($req_no);
				$req_date = $item[0]['request_date'];
				$req_type = $item[0]['type_name'];
				$req_to_costcenter = $item[0]['to_costcenter'];
				$user_request = $item[0]['username'];

				$item =  $this->Request_model->request_total_book_value($req_no);
				$total_book_value = $item[0]['total_book_val'];
				$qty_asset = $item[0]['qty_asset'];

			$this->config->load('grocery_crud');
			// $this->config->set_item('grocery_crud_dialog_forms',true);
			$this->config->set_item('grocery_crud_default_per_page',20);
		

			$output1 = $this->request_detail_by_id($req_no);
	
			$url_request_list = base_url('fa_con/request_new/new_request');


			$js_files = $output1->js_files;
			$css_files = $output1->css_files;
			$output = "<h2>Request No. $req_no | Date Request $req_date</h2>
			<h2>For $req_type | To Costecnter $req_to_costcenter | By user : $user_request</h2>
			<h2>Total Value : $total_book_value Baht | Qty :  $qty_asset Assets</h2>
			</hr><hr>
			<a href='$url_request_list' target='_parent'><button class='btn btn-primary'>Back to Request List</button></a>
			<br><br>
			".$output1->output;
	
			$this->_example_output_request_detail((object)array(
					'js_files' => $js_files,
					'css_files' => $css_files,
					'output'	=> $output
			));
	}

	public function request_detail_by_id($req_no){

		$req_no = $this->session->userdata('req_num');
		$it_asset = '1';
	
		// echo ("call back insert->");
		// echo ($this->session->userdata('req_num'));
		// echo ("</br>");
		// echo ($req_no);
		// echo (site_url(strtolower("/fa_con/".__CLASS__."/".__FUNCTION__)));
		// echo ("</br>");
		// echo (site_url(strtolower("/fa_con/".__CLASS__."/")));

		$crud = new grocery_CRUD();
		$crud->set_table('t_request_detail');
		$crud->where('request_no',$req_no);

		// check user role 
		// 1. location admin view asset all in owner location only
		// 2. it admin  view asset all location + it asset only (fa_it_asset = 1)
		// 3. account admin view asset all + all location
		
		

		$crud->set_primary_key('asset_id','v_asset_by_dept_by_req');
		$crud->set_relation('asset_id','v_asset_by_dept_by_req','{asset_no}-{asset_description}-{invent_no} / {costcenter} ', ['v_asset_by_dept_by_req.req_number =' => $req_no],'asset_id asc');
		$crud->where('req_number',$req_no);

		$crud->display_as('asset_id','Fix Asset');
		
	
		$crud->set_subject('Request Detail List');

		$crud->set_crud_url_path(site_url(strtolower("/fa_con/".__CLASS__."/".__FUNCTION__)),site_url(strtolower("/fa_con/".__CLASS__."/request_detail/")));

		$crud->callback_after_insert(array($this, 'update_req_number'));

		$crud->field_type('request_no', 'hidden');

		$crud->unset_columns(array('request_no'));

		$output = $crud->render();

		if($crud->getState() != 'list') {
			$this->_example_output($output);

		} else {
			return $output;
		}
	
	}

	public function update_req_number($post_array,$primary_key)
	{
			
			$req_number =$this->session->userdata('req_num');
			
			$sql = " update t_request_detail SET
			request_no = '$req_number'
			where request_detail_id = '$primary_key'
			";

            $query = $this->db->query($sql);
	
			return true;
	}



}

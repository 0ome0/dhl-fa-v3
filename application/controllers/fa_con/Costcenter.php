<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Costcenter extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->database();
		$this->load->helper('url');

		$this->load->library('grocery_CRUD');
		// $this->load->model('itasset/Vendor_model', 'Vendor_model');
	}

	// public function _example_output($output = null)
	// {
	// 	$this->load->view('fix-asset/layout-asset_type.php',(array)$output);
	// }

	public function offices()
	{
		$output = $this->grocery_crud->render();

		$this->_example_output($output);
	}

	public function index()
	{
		$this->_example_output((object)array('output' => '' , 'js_files' => array() , 'css_files' => array()));
	}

	public function costcenter_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_costcenter');
			$crud->set_relation('status','t_status','status',null,'status desc');
			$crud->display_as('status','status');
			$crud->set_subject('Costcenter');

			$crud->required_fields('costcenter_id');
			$crud->required_fields('status');

            $crud->field_type('created_date', 'hidden', 3);
            $crud->field_type('costcentr_name', 'hidden', 3);
			$crud->columns('costcenter_id');

			// $crud->set_field_upload('center_map','assets/uploads/files');
			// // $crud->field_type('center_code','readonly');

			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('fa/costcenter.php',(array)$output);
    }
    
    public function country_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_country');
			$crud->set_relation('status','t_status','status',null,'status desc');
			$crud->display_as('status','status');

			// role = 5
			$crud->set_relation('user_country_manager','ci_users','{username} ( {firstname} {lastname} )',array('role' => '5'),'firstname asc');
			$crud->display_as('id','User Country Manager');

			$crud->set_subject('Country');

			$crud->required_fields('country_id');
			$crud->required_fields('status');

            $crud->field_type('created_date', 'hidden', 3);

			$crud->columns('country_id','country_name');

			// $crud->set_field_upload('center_map','assets/uploads/files');
			// // $crud->field_type('center_code','readonly');

			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('fa/country.php',(array)$output);
    }
    
    public function department_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_department');
			$crud->set_relation('status','t_status','status',null,'status desc');
			$crud->display_as('status','status');
			$crud->set_subject('Department / Activity Function');

			// role = 4
			$crud->set_relation('user_department_head','ci_users','{username} ( {firstname} {lastname} )',array('role' => '4'),'firstname asc');
			$crud->display_as('id','User Department Head');

			$crud->required_fields('department_id','function_description','function_name');
			$crud->required_fields('status');

            $crud->field_type('created_date', 'hidden', 3);

			$crud->columns('department_id','function_description','function_name');

			// $crud->set_field_upload('center_map','assets/uploads/files');
			// // $crud->field_type('center_code','readonly');

			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('fa/department.php',(array)$output);
    }
    
    public function location_management()
	{
			$crud = new grocery_CRUD();

			$crud->set_theme('flexigrid');
			$crud->set_table('t_servicecenter');
			$crud->set_relation('status','t_status','status',null,'status desc');
            $crud->display_as('status','status');
            $crud->display_as('sc_id','location code');
			$crud->display_as('sc_name','location name');
			
			// role = 2 as location admin
			$crud->set_relation('user_location_admin','ci_users','{username} - {firstname} {lastname} ',array('role' => '2'),'firstname asc');
			$crud->display_as('id','User Location Admin');

			// role = 2 as location manager
			$crud->set_relation('user_location_manager','ci_users','{username} - {firstname} {lastname} ',array('role' => '5'),'firstname asc');
			$crud->display_as('id','User Location Manager');

			$crud->set_subject('Location / Service Center');

			$crud->required_fields('sc_id','sc_name');
			$crud->required_fields('status');

            $crud->field_type('created_date', 'hidden', 3);

			$crud->columns('sc_id','sc_name','user_location_admin','user_location_manager');

			// $crud->set_field_upload('center_map','assets/uploads/files');
			// // $crud->field_type('center_code','readonly');

			$output = $crud->render();

			// $this->_example_output($output);
			$this->load->view('fa/location.php',(array)$output);
	}

}

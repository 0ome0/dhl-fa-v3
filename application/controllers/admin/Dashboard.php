<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Dashboard extends MY_Controller {
		public function __construct(){
			parent::__construct();
			$this->load->model('fix-asset/dashboard_model', 'dashboard_model');
		}

		public function dashboard_admin (){
			// $whse = $this->session->userdata('whse');
			// $uesr_role = $this->session->userdata('role');

			redirect(base_url('admin/dashboard/dashboard_user'), 'refresh');

			// if($uesr_role == '2') {
			// 	redirect(base_url('admin/dashboard/dashboard_user'), 'refresh');
			// }

			// $data['alert_count_lower_minimum_all_whse'] =  $this->dashboard_model->get_alert_count_lower_minimum_all_whse();
			// $data['alert_lower_minimum_all_whse'] =  $this->dashboard_model->get_alert_lower_minimum_all_whse();
			// $data['count_transfer_receive_pending_all_whse'] =  $this->dashboard_model->get_count_transfer_receive_pending_all_whse();
			// $data['transfer_receive_pending_all_whse'] =  $this->dashboard_model->get_transfer_receive_pending_all_whse();
			
			

			// $this->load->view('admin/layout-dashboard-admin.php',$data);
		}

		public function dashboard_user (){
			$whse = $this->session->userdata('whse');
			$uesr_role = $this->session->userdata('role');

			$data['number_req_submit'] =  $this->dashboard_model->get_count_request_submit_status();
			$data['number_req_transfer'] =  $this->dashboard_model->get_count_request_transfer();
			$data['number_req_disposal'] =  $this->dashboard_model->get_count_request_disposal();

			// $data['asset_expired'] =  $this->dashboard_model->get_asset_expired();
			// $data['asset_expired_1'] =  $this->dashboard_model->get_asset_expired_in_1();
			// $data['asset_expired_2'] =  $this->dashboard_model->get_asset_expired_in_2();
			// $data['asset_expired_3'] =  $this->dashboard_model->get_asset_expired_in_3();

			// $this->load->view('fix-asset/layout-dashboard-center.php',$data);
			$this->load->view('fa/dashboard_view',$data);
		}

		public function dashboard_service_center (){
			$whse = $this->session->userdata('whse');
			$uesr_role = $this->session->userdata('role');
			
			$data['number_all_item'] =  $this->dashboard_model->get_count_total_asset_all_service_center();
			$data['number_item_asset'] =  $this->dashboard_model->get_count_total_asset_by_service_center();
			
			$this->load->view('fa/dashboard_view_service_center',$data);
		}


		// public function index(){
		// 	$data['view'] = 'admin/dashboard/index';
		// 	$this->load->view('admin/layout', $data);
		// }

		// public function index2(){
		// 	$data['view'] = 'admin/dashboard/index2';
		// 	$this->load->view('admin/layout', $data);
		// }
	}

?>	
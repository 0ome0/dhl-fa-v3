<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Qr_scan extends CI_Controller {

    public function __construct(){
        parent::__construct();
        // $this->load->model('admin/auth_model', 'auth_model');
        // $this->load->model('admin/transaction_model', 'transaction_model');
        // $this->load->model('fix-asset/Fix_asset_model', 'Fix_asset_model');
        $this->load->model('fa/Fix_asset_model', 'Fix_asset_model');

    }

	public function index()
	{
		redirect(base_url('admin'));
    }
    
    public function scan(){
        $qr_code_id  = $this->input->get('qr_code_id');


        $result = $this->Fix_asset_model->get_item_asset_detail($qr_code_id);
      
        if ($result == TRUE) {
            // echo json_encode($result);

            // $this->load->model('itasset/Item_asset_model', 'Item_asset_model');
            // $data['item_detail'] =  $this->Item_asset_model->get_item_by_id($id);
            $data['item_detail'] = $result;

            // $this->load->view("itasset/layout-qr_result.php", $data);
            $this->load->view("fix-asset/layout-qr_result_box.php", $data);

        }
        else{
            $msg = 'Not Found Data';
            echo $msg;
        }

    } 

    // public function login(){

    //     $username = $this->input->get('username');
    //     $password = $this->input->get('password');
        
    //     $data = array(
    //         'username' => $username,
    //         'password' => $password 
    //         );

    //         $result = $this->auth_model->login($data);
    //         if ($result == TRUE) {
    //             $admin_data = array(
    //                 'admin_id' => $result['id'],
    //                  'name' => $result['username'],
    //                  // add warehouse and user profile set session
    //                  'whse' => $result['center_code'],
    //                  'email' => $result['email'],
    //                  'firstname' => $result['firstname'],
    //                  'lastname' => $result['lastname'],
    //                  'role' => $result['role']
    //                 //  'is_admin_login' => TRUE
    //             );

    //             echo json_encode($admin_data);
    //         }
    //         else{
    //             $msg = 'Invalid Email or Password!';
    //             echo $msg;
    //         }
    // }

    public function gen_newqr(){

        $start_asset_id = 1;

        // echo $start_asset_id ;
        
        for($i=2200;$i<=2500;$i++)
        
        {
       
            $id  =  $i;
        
            $result = $this->Fix_asset_model->get_item_by_id_and_not_have_qr($id);
            $data['item_detail'] = $result;

            // print_r( $data['item_detail']);

            if ($result == TRUE) {
                
                echo $i;
                echo '<br>';

                $qr_code = strtoupper(uniqid());
				// $barcode_id = '*'.$barcode.'*';

				$this->load->library('ciqrcode');

                //$qr_gen = base_url()."api/qr_scan/scan?qr_code_id=".$qr_code;
                $qr_gen = "https://service-imsp.com/dhl-fa-v3/api/qr_scan/scan?qr_code_id=".$qr_code;

				// $params['data'] = $qr_code;
				$params['data'] = $qr_gen;
				$params['level'] = 'H';
				$params['size'] = 10;
				$params['savename'] = "assets/images/qrcode/$qr_code.png";

				$qrcode_image = "$qr_code.png" ;
                $this->ciqrcode->generate($params);
                
                // to save 
                $qr_code_id = $qr_code;
                $qr_code_img = $qrcode_image;
                $asset_id = $i;

                //  echo $qr_code_id;
                 
                
                $sql = "update t_fa 
                set qr_code_id = '$qr_code_id' , qr_code_img = '$qr_code_img'
                where asset_id = '$asset_id' ";
			    $query = $this->db->query($sql);

            }
            else{
                $msg = '';
                //echo $id; 
                echo $msg;
            }

        
        }


                   
    } 
    
    
}
